@extends('frontend.layouts.app')

@section('title', app_name() . ' | Job List')

@section('content')


    <div class="row">

        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('Job list') }}</div>
                    
                <div class="panel-body">                   
                    <div class="search_area adv_jobs">
                        <form method="get" accept-charset="UTF-8" class="searchform" autocomplete="off" action="{{url('/myjob?')}}">

                            <h3>Advanced Job Search</h3>
                            <h4>Find Jobs</h4> 
                       
                        <div class="form-group">
                             <label> With none of these words</label>
                            <input id="as_not" name="as_not" size="35" maxlength="512" value="" autocomplete="off" type="text">
                            </div>
                        <div class="form-group">
                             <label> From this company</label>
                            <input id="as_cmp" name="as_cmp" size="35" maxlength="512" value="" autocomplete="off" type="text">
                            </div>
                        <div class="form-group">
                            <label> Show jobs from</label>
                            <select id="st" name="st">
                            <option value="">All web sites</option>
                            <option value="jobsite">Job boards only</option>
                            <option value="employer">Employer web sites only</option>
                        </select>
                        </div>
                        <div class="form-group">

                        <label> </label>

                         <input id="norecruiters" name="sr" value="directhire" type="checkbox">&nbsp;<label for="norecruiters">Exclude staffing agencies</label>

                         </div>
                         <div class="form-group">
                            <label> Location</label>
                            <span class="adv_lo">
                                <input id="" name="filter_type" size="35" maxlength="512" value="adv" autocomplete="off" type="hidden">
                             <input id="where" name="lo" size="35" maxlength="512" value="" autocomplete="off" type="text">
                               <div id="lo-box" class="suggesstion-box"></div>
                             </span>
                        </div>
                            <div class="form-group">
                            <label> Age - Jobs published</label>
                           
                            <select id="fromage" name="fromage">
                             <option value="any">anytime</option>
                            <option value="15">within 15 days</option>
                            <option value="7">within 7 days</option>
                            <option value="3">within 3 days</option>
                            <option value="1">since yesterday</option>
                            <option value="last">since my last visit</option>
                        </select>
                        </div>

                         <div class="col-md-12">
<div class="col-md-3"></div>
                            <div class="col-md-9">
                         <span class="inwrapBorder" style="width:auto;padding-right:0;">
                                <span class="inwrapBorderTop"> <input type="submit"  value="Find Jobs" class="input_submit" ></span>
                            </span>
                        </div>

                        </div>
                        












                           


                          <!--   <label class="label_st" for='what'> What</label>
                            <span class="inwrap">
                                <input type="text" name="q" placeholder="What" value="<?php if(isset($q)) { echo $q; }?>" required="required" class="input_text" id="what" autocomplete="off">
                                <div id="q-box" class="suggesstion-box"></div>

                            </span>
                            <label class="label_st">Job Type</label>
                            <span class="inwrap">
                                <input type="text" name="jt" placeholder="Job Type" value="" class="input_text">
                            </span>
                            <label class="label_st">Where</label>
                            <span class="inwrap">
                                <input type="text" name="lo" placeholder="Where" value="<?php if(isset($l)) { echo $l; }?>" class="input_text" autocomplete="off" id="where">
                                 <div id="lo-box" class="suggesstion-box"></div>
                                 <input type="hidden" name="sr" placeholder="What" value="directhire" required="required" class="input_text" id="sr">

                            </span>
                            <span class="inwrapBorder" style="width:auto;padding-right:0;">
                                <span class="inwrapBorderTop"> <input type="submit"  value="Find Jobs" class="input_submit" ></span>
                            </span>
                            <a href="">Advanced Search</a> -->
                        </form>
                   
                    </div>
                </div><!-- panel body -->

            </div><!-- panel -->

        </div><!-- col-md-8 -->

    </div><!-- row -->
      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
 var termTemplate = "<span class='ui-autocomplete-term'>%s</span>";
   $(document).ready(function() {
         $("#what").keyup(function(e){
            if(this.value!=""){
                $.ajax({
                  type:"get",
                  url: "<?php echo url('getjobtitle'); ?>?q="+this.value,
                  datatype:"json",
                  success: function(response) 
                  {  
                    $("#q-box").show();
                    $("#q-box").html(response);
                    $("#what").css("background","#FFF");
                  }
                });
            }
        });
         $("#where").keyup(function(e){
            if(this.value!=""){
                $.ajax({
                  type:"get",
                  url: "<?php echo url('getjoblocation'); ?>?q="+this.value,
                  datatype:"json",
                  success: function(response) 
                  {  
                    $("#lo-box").show();
                    $("#lo-box").html(response);
                    $("#where").css("background","#FFF");
                  }
                });
            }
        });
    });
function fillq(val) {
  var val=  val.replace(/\-/g," ");
$("#what").val(val);
$("#q-box").hide();
}
function filllo(val) {
     var val=  val.replace(/\-/g," ");
$("#where").val(val);
$("#lo-box").hide();
}


</script>@endsection