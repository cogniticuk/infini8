@extends('frontend.layouts.app')
@section('content')
    <div class="dashboard-infini">
    <div class="dashboard-header acc_heading">        
		<div class="container">
			<div class="job_table_view" >  
				<div class="content-header" style="float: left;">
					<h3>
						{{ trans('Top Trends') }}
					</h3>
				</div>
			</div>
			<div>
		    	<div class="cat_section">	    
		    		<select name="sel_cat" onchange="redirectpage(this.value);">
		    		@if(count($all_category)>0)
			    		@foreach($all_category as $val)
		    				<?php $links=url('trends/').$val['category_url'].'/'.$val['category_name'];?>
			    			<option value="{{$links}}" <?php if($val['category_name']==$ctaname){ echo " selected='selected'";}?>>{{$val['category_name']}}</option>
			    		@endforeach
		    		@endif
		    		</select>
		    	</div>
		        <div class="heading_section"><h4>{{$ctaname}}</h4></div>
	        </div>
		 <div class="col-md-12 col-sm-12 Topblock">
		  <div class="account_box">
		   <div class="col-md-6 col-sm-6">
		    <h3>Job postings</h3>
			<p>Average job postings this month:</p>
			<h2>@if(isset($items[0]['job_postings_number']) && $items[0]['job_postings_number']!=""){{$items[0]['job_postings_number']}}@else {{-----}}@endif</h2>
		   </div>
		   <div class="col-md-6 col-sm-6">
		    <h3>Change in job market share in percentage points</h3>
			<p >Compared to last month:</p>
           	@if($items[0]['cat_ch_positive']!="")
           <h5 class="cat_postive_up">	{{$items[0]['cat_ch_positive']}}
	        @elseif($items[0]['cat_ch_negative']!="")
	            <h5 class="cat_negative_down">{{$items[0]['cat_ch_negative']}}
	        @else
	            <h5 class="catnumbes">{{'----'}}
	        @endif
              </h5>
			<p style="margin-right: 20px;">Compared to last year:</p>			
           	@if($items[0]['cat_ch_positivey']!="")
           <h5 class="cat_postive_up">	{{$items[0]['cat_ch_positivey']}}
	        @elseif($items[0]['cat_ch_negativey']!="")
	            <h5 class="cat_negative_down">{{$items[0]['cat_ch_negativey']}}
	        @else
	            <h5 class="catnumbes">{{'----'}}
	        @endif
              </h5>
		   </div>
		  </div>
		 </div>
		 <div class="col-md-4 col-sm-4 Topblock">
		  <div class="Top-List">      
			<h2 class="top-header">Top job titles per click</h2>
			@if(isset($perclickdata[1]) && $perclickdata[1]!="")
				@foreach($perclickdata[1] as $k=>$v)
				<div class="job_title"><a href="<?php echo url('myjob?q=').$v."&l=&viewtype=s" ?>" target="_blank">{{$v}}</a><span>{{$perclickdata[2][$k]}}</span></div>
				@endforeach
			@endif
		  </div>
		 </div>
          <div class="col-md-4 col-sm-4 Topblock">
          <div class="Top-List">      
            <h2 class="top-header">Top search terms per click</h2>
        	@if(isset($perclickdata[3]) && $perclickdata[3]!="")
				@foreach($perclickdata[3] as $k=>$v)
					<div class="job_title"><a href="<?php echo url('myjob?q=').$v."&l=&viewtype=s" ?>" target="_blank">{{$v}}</a><span>{{$perclickdata[4][$k]}}</span></div>
				@endforeach
			@endif
          </div>
         </div>
          <div class="col-md-4 col-sm-4 Topblock">
          <div class="Top-List">      
            <h2 class="top-header">Top job locations per click</h2>
        	@if(isset($perclickdata[5]) && $perclickdata[5]!="")
				@foreach($perclickdata[5] as $k=>$v)
					<div class="job_title"><a href="<?php echo url('myjob?q= ')."&l=".$v."&viewtype=s" ?>" target="_blank">{{$v}}</a><span>{{$perclickdata[6][$k]}}</span></div>
				@endforeach
			@endif
          </div>
         </div>
		</div>
    </div>            
    </div>
    <script>
    	function redirectpage(links)
    	{
    		window.location.href=links;
    	}
    </script>
@endsection