@extends('frontend.layouts.app')

@section('content')
<div class="container">
<div class="job_table_view">  
  <div class="">  
    <div class="content-header">
        <h3>
            {{ trans('Guided Analysis') }}
        </h3>
     </div>
  </div>
</div>
<div class="job_table_view job-view-sec">  
  <div class="">  
    <div class="box box-success">  
      <div class="box-body">
        <div class="table-responsive">
          <table class="table table-condensed table-hover" id="example">
            <thead>
               <tr>
                <th style="width: 5%; ">Category title</th>
                <th style="width: 5%; text-align: center;">Avg job posted</th>
                <th style="width: 5%; text-align: center;">Compared to last month</th>
                <th style="width: 5%; text-align: center;">Compared to last year</th>
               </tr>
            </thead>
            <tbody>
             <?php 
              if(count($cat_data)>0)
                {
                  foreach($cat_data as $key=>$val){        
              ?>
                 <tr>
                    <td style="width: 5%; "><span style="font-weight: bold"><a href='<?php echo url('myjob?q=').$val->cat_name."&l=&viewtype=s" ?>' target='_blank'>{{ $val->cat_name }}</a></span></td>
                    <td style="width: 5%; text-align: center;" class="companyname-color"><span>{{ $val->jobposting}}</a></span></td>
                    <td style="width: 5%; text-align: center;">
                      <span style="text-align: center;" class="">
                        @if($val->last_month!="")
                          @if($val->last_month>=0)
                            {{$val->last_month}}
                          @else
                            <span class="cat3">
                              <span class="cat_negative_down1">

                              {{$val->last_month}}
                            </span>
                            </span>
                          @endif                        
                        @else
                            {{'----'}}
                        @endif
                      </span>
                    </td>
                    <td style="width: 5%; text-align: center;">
                      <span style="text-align: center;">
                        @if($val->last_year!="")
                          {{$val->last_year}}
                        @else
                          {{'----'}}
                        @endif
                      </span>
                    </td>              
                 </tr>
              <?php }
              }else{
                echo "<div class='re-not-found'>Result could not be found.</div>";
              }?>
           </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div> 
</div> 
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <link media="all" type="text/css" rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<script>
 $(document).ready(function() {
    $('#example').DataTable( {
        "order": [[ 3, "desc" ]],
        "bPaginate": false
    } );
} ); 
</script>
@endsection
