@extends('frontend.layouts.app')

@section('title', app_name() . ' | Find Leads')


@section('content')
<div class="job_table_view">  
<div class="col-md-12">  
<div class="content-header">
    <h3>
        {{ trans('Find Leads') }}
    </h3>
   </div>
</div>
</div>
    
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<style>
.snip .no-wrap{
  display: none;
}
.company a{
  display: none;
}
.summary{
  display: none;
}
.result-link-source{
  display: none;
}
</style>
  <div class="search_area container_job_list">
    <form method="get" action="" accept-charset="UTF-8" class="searchform" autocomplete="off">
      <!-- <label class="label_st" for='what'> What</label> -->
      <div class="row">
      <span class="inwrap col-sm-4">
      <div class="icon-addon addon-md">
                    <input type="text" name="q" placeholder="Search jobs, companies" value="<?php if(isset($q)) { echo str_replace('-',' ',$q); }?> <?php if(isset($_REQUEST['as_cmp'])) { echo 'Company:'.str_replace('-',' ',$_REQUEST['as_cmp']); }?><?php if(isset($_REQUEST['as_and'])) { echo str_replace('-',' ',$_REQUEST['as_and']); }?>" required="required" class="input_text" id="what" autocomplete="off">
                    <i class="fa fa-suitcase" aria-hidden="true"></i>
                </div>
       
        <div id="q-box" class="suggesstion-box"></div>
      </span>                           
      <!-- <label class="label_st">Where</label> -->
      <span class="inwrap col-sm-4">

      <div class="icon-addon addon-md">
                    <input type="text" name="lo" placeholder="London" value="<?php if(isset($l)) { echo $l; }?>" class="input_text" autocomplete="off" id="where">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                </div>
        
        <div id="lo-box" class="suggesstion-box"></div>
        <!-- <input type="hidden" name="sr" placeholder="What" value="directhire" required="required" class="input_text" id="sr"> -->
      </span>
      <span class="inwrapBorder col-sm-2" style="width:auto;padding-right:0;">
        <span class="inwrapBorderTop"> <input type="submit"  value="Search" class="input_submit" ></span>
      </span>
      <div class="accordion-heading col-sm-2">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne"><img src="{{ asset('img/search-add.png') }}"></a>
      </div>
    </div>
    </form>
    <div class="title_serach">
      @if(count($rr_title)>0 && $rr_title!="")
      <div id="TITLE_rbo" class="rbsrbo"> 
        <ul class="rbList"> 
          <?php $i=0;
          foreach($rr_title as $k=>$tval) {
            $class="";
            if($i>4){
              $class="moreLi";
            }?>
          <li class="<?php echo $class;?>" onmousedown="rbptk('rb', 'ttl', '1');">    
           <a href="<?php echo url('myjob?/').$tval['url']; ?>" title="<?php echo $tval['title']; ?>"><?php echo $tval['title']; ?></a> 
         </li> 
         <?php $i++;
       }?>
       </ul>
       <?php if(count($rr_title)>4){?>
       <div class="more_link">
                 <span tabindex="0" style="display:none" class="morespan">more »</span>
                  <span tabindex="0" class="lessspan">less »</span>
               </div>
       <?php }?>
     </div>
     @endif
      <?php //echo $rb_title;?>
    </div>
    <div class="accordion" id="accordion2">
      <div class="accordion-group">
        <div id="collapseOne" class="accordion-body collapse">
          <div class="accordion-inner">
          <form method="get" accept-charset="UTF-8" class="searchform" autocomplete="off" >
            <div class="ad_search_area adv_jobs">
              
              <h3>Advanced Lead Search</h3>
              <h4>Find Leads</h4> 
            <div class="col-md-6 ad_search-prt">
              <div class="form-group">
                <label class="col-md-5"> With all of these words</label>
                <input  class="col-md-6" id="as_and" name="as_and" size="35" maxlength="512" autocomplete="off" type="text" value="<?php if(isset($_REQUEST['as_and'])) { echo $_REQUEST['as_and']; }?> <?php if(isset($_REQUEST['q'])) { echo $_REQUEST['q']; }?> ">
              </div>
              </div>
 <div class="col-md-6 ad_search-prt">
              <div class="form-group">
                <label class="col-md-5"> Location</label>
                <span class="adv_lo col-md-6 ">
                  <input id="" name="filter_type" size="35" maxlength="512" value="adv" autocomplete="off" type="hidden" > 
                  <input id="where1" name="lo" size="35" maxlength="512" autocomplete="off" type="text" value="<?php if(isset($_REQUEST['lo'])) { echo $_REQUEST['lo']; }?>">
                  <div id="lo1-box" class="suggesstion-box"></div> 
                </span>
              </div>
              </div>
                <div class="col-md-6 ad_search-prt">
              <div class="form-group">
                <label class="col-md-5"> Show jobs of type</label>
                <select  class="col-md-6" id="jt" name="jt">
                    <option value="all">All job types</option>
                    <option value="temporary" <?php if(isset($_REQUEST['jt']) && $_REQUEST['jt']=="temporary") { echo " selected='selected'"; }?>>Temporary</option>
                    <option value="fulltime" <?php if(isset($_REQUEST['jt']) && $_REQUEST['jt']=="fulltime") { echo " selected='selected'"; }?>>Full-time</option>
                    <option value="parttime" <?php if(isset($_REQUEST['jt']) && $_REQUEST['jt']=="parttime") { echo " selected='selected'"; }?>>Part-time</option>
                    <option value="contract" <?php if(isset($_REQUEST['jt']) && $_REQUEST['jt']=="contract") { echo " selected='selected'"; }?>>Contract</option>
                    <option value="internship" <?php if(isset($_REQUEST['jt']) && $_REQUEST['jt']=="internship") { echo " selected='selected'"; }?>>Internship</option>
                </select>
              </div>
              </div>
              <!-- <div class="col-md-6 ad_search-prt">
              <div class="form-group">
                <label class="col-md-5"> From this company</label>
                <input  class="col-md-6" id="as_cmp" name="as_cmp" size="35" maxlength="512"  autocomplete="off" type="text" value="<?php if(isset($_REQUEST['as_cmp'])) { echo $_REQUEST['as_cmp']; }?>">
              </div>
              </div> -->
              <div class="col-md-6 ad_search-prt">
              <div class="form-group">
                <label class="col-md-5"> Show jobs from</label>
                <select  class="col-md-6" id="st" name="st">
                  <option value="">All web sites</option>
                  <option value="jobsite"<?php if(isset($_REQUEST['st']) && $_REQUEST['st']=="jobsite") { echo " selected='selected'"; }?>>Job boards only</option>
                  <option value="employer"<?php if(isset($_REQUEST['st']) && $_REQUEST['st']=="employer") { echo " selected='selected'"; }?>>Employer web sites only</option>
                </select>
              </div>
              </div>
              <div class="col-md-6 ad_search-prt">
              <div class="form-group col-md-12">
             <label for="norecruiters">Exclude staffing agencies</label> &nbsp;
                <input id="norecruiters" name="sr" value="directhire" type="checkbox" <?php if(isset($_REQUEST['sr']) && $_REQUEST['sr']=="directhire") { echo " checked='checked'"; }?>>
              </div>
              </div>
           
              <div class="col-md-6 ad_search-prt">
              <div class="form-group">
                <label class="col-md-5"> Age - Jobs published</label>
                <select  class="col-md-6" id="fromage" name="fromage">
                  <option value="any" <?php if(isset($_REQUEST['fromage']) && $_REQUEST['fromage']=="any") { echo " selected='selected'"; }?>>anytime</option>
                  <option value="15" <?php if(isset($_REQUEST['fromage']) && $_REQUEST['fromage']=="15") { echo " selected='selected'"; }?>>within 15 days</option>
                  <option value="7" <?php if(isset($_REQUEST['fromage']) && $_REQUEST['fromage']=="7") { echo " selected='selected'"; }?>>within 7 days</option>
                  <option value="3" <?php if(isset($_REQUEST['fromage']) && $_REQUEST['fromage']=="3") { echo " selected='selected'"; }?>>within 3 days</option>
                  <option value="1" <?php if(isset($_REQUEST['fromage']) && $_REQUEST['fromage']=="1") { echo " selected='selected'"; }?>>since yesterday</option>
                  <option value="last" <?php if(isset($_REQUEST['fromage']) && $_REQUEST['fromage']=="last") { echo " selected='selected'"; }?>>since my last visit</option>
                </select>
              </div>
              </div>
                <div class="ad-right-button">
                  <div class="col-md-12">
                    <span class="inwrapBorder" style="width:auto;padding-right:0;">
                    <span class="inwrapBorderTop"> <input value="Search" class="input_submit" type="submit"></span>
                    </span>
                  </div>
                </div>
                <div class="button-ad-sea"><a class="btn pull-left"> Save filter <i class="fa fa-angle-double-right" aria-hidden="true"></i></a> <a class="btn pull-right"> <i class="fa fa-times" aria-hidden="true"></i> Reset filter </a></div>
            </div>
             </form>
          </div>
        </div>
      </div>
    </div>
  </div>

<div class="job_table_view job-view-sec">  
<div class="col-md-12">  
 <div class="box box-success">
  <div class="box-header with-border">
   <div class="dataTables_info right_total"> Showing <b id="tot_count"><?php echo $pag_cont_text;?></b> </div>
   </div>
  <div class="box-body">
   <div class="table-responsive">
    <table class="table table-condensed table-hover">
     <thead>
		 <tr>
		  <th>Job Title</th>
		  <th>Company</th>
		  <th>Location</th>
		  <th>Job Source </th>
		  <!--<th>Job Type</th>
		  <th>Salary range</th>-->
		  <th>Job Updated</th>
		  <th>Action</th>
		 </tr>
	</thead>
	 <tbody id="dataappend">
 <?php 
  if(count($final)>0)
    {
      $locations="";
      if($l!="")
      {
        $locations=$l;
      }
      foreach($final as $key=>$val){
        if(isset($val['title'])){
            $title = $val['title'];
        }else{
          $title = "" ;
        }
        if(isset($val['companyname']) && $val['companyname']!=""){
          $companyname = $val['companyname'];
          $companyname1 = $val['companyname'];
        }
        else{
          $companyname = ""; 
          $companyname1 = '';
        }  
        ?>
    		 <tr>
    		  <td><span><a href='https://www.indeed.co.uk/<?php echo $val['link'] ;?>' target='_blank'>{{ $title }}</a></span></td>
    		  <td class="companyname-color"><span><a href="<?php echo url('myjob?as_cmp=').str_replace(' ', '-', $companyname1 )."&l=".$locations."&viewtype=v" ?>"  target="_blank">{{ $companyname}}</a></span></td>
    		  <td><span>{{$val['location']}}</span></td>
    		  <td><span>{{$val['jobsource']}}</span></td>
    		  <td><span><?php if(isset($val['postdate']) && $val['postdate']!=""){
    			  echo $val['postdate'];
    			} ?></span></td>
    		  <td>
            <a class="btn btn-xs btn-info" href='https://www.indeed.co.uk/<?php echo $val['link'] ;?>' target='_blank'><img src="{{ asset('img/view.png') }}"></a>
            <a class="btn btn-xs btn-primary" href='https://www.linkedin.com/search/results/people/?company={{$companyname1}}&origin=GLOBAL_SEARCH_HEADER' target='_blank'><img src="{{ asset('img/indded.png') }}"></a>
            <a class="btn btn-xs btn-danger" href='https://www.google.com/search?q={{$companyname1}} contact' target='_blank'><img src="{{ asset('img/google.png') }}"></a>
            <a class="btn btn-xs btn-info" href="<?php echo url('getresumes?q=').str_replace(' ', '-', $title )."&l=".$val['location']."&viewtype=s" ?>" target='_blank'><img src="{{ asset('img/users.png') }}"></a>
            <?php 
                if(!Auth::user()) 
                  {
                    echo "<a class='btn btn-xs btn-warning' href='javascript:void(0);' onclick='nouser();'><img src=".asset('img/plophy.png')." alt='Save' title='Save'></a>";
                  }else{
                    $checkjobs=checkjob($val['link']);
                    if($checkjobs=="true")
                    {
                        echo "<a class='btn btn-xs btn-warning' id='fav_".$key."'class='fav_un' href='javascript:void(0);' onclick=favouritejob('".base64_encode($val['link'])."','unfav',".$key.") title='saved'><img src=".asset('img/plophy.png')." alt='Saved' title='Saved'></a>";
                    }else{
                    echo "<a class='btn btn-xs btn-warning' id='fav_".$key."'class='fav_un' href='javascript:void(0);' onclick=favouritejob('".base64_encode($val['link'])."','fav',".$key.") title='save'><img src=".asset('img/plophy.png')." alt='Save' title='Save'></a>";
                    }
                  }?></td>
    		 </tr>
    		<?php }
            }else{
              echo "<div class='re-not-found'>Result could not be found.</div>";
            }?>
	 </tbody>
</table>
<div class="dataTables_info"> Showing <b id="tot_count"><?php echo $pag_cont_text;?></b></div>
</div>
</div>
<div class="animation_image" style="display:none" align="center">
    <img src="{{ asset('img/ajax-loader.gif') }}">
</div>
</div>
</div>
</div> 
<input type="hidden" name ="gropcount" id="gropcount" value="">
       
<?php $total_groups= $noofpage;?> 
<?php $reqda= base64_encode($_SERVER['REQUEST_URI']);?> 
<script>
var track_load = 1; 
var loading  = false;
var total_groups = <?php echo $total_groups;?>;
$(function(){
    $(window).scroll(function(){
        var gropcount =$("#gropcount").val();
        if(gropcount)
        {
            total_groups= parseInt(gropcount);
        }
        if($(window).scrollTop() + $(window).height() == $(document).height())
        {
            if(track_load <= total_groups && loading==false)
            {       
                loading = true;
                $('.animation_image').show();
                $.get("{!!URL::to('getfilterJob')!!}",{'group_no': track_load,'reqda': '<?php echo $reqda?>'}, function(data){
                  var res_data=data.split("[{[");
                    $("#dataappend").append(res_data[0]);
                    $("#tot_count").html(res_data[1]); 
                    $('.animation_image').hide(); 
                    track_load++;
                    loading = false;
                }).fail(function(xhr, ajaxOptions, thrownError) { 
                   swal(
                      'Oops...',
                      thrownError,
                      'error'
                    )
                    $('.animation_image').hide();
                    loading = false;
                });
            }
        }
    });
});
  function nouser()
  {
    swal(
    'Oops...',
    'Please logged in to favourite job',
    'error'
    )
  }
  function favouritejob(strval,types,ids)
  {     
  if(strval!=""){
       $.ajax( {
          url: "<?php echo url('fav_job'); ?>",
          dtype:"get",
          data: {
            term: strval,type:types
          },
          success: function( data ) {
            if(types=="fav")
            {
              $('#fav_'+ids).html('saved');
              $('#fav_'+ids).attr('onclick',"favouritejob('"+strval+"','unfav',"+ids+")");
                swal(
                'Success',
                'Job Successfully Saved',
                'Success'
                )
            }else{
              $('#fav_'+ids).html('save');
              $('#fav_'+ids).attr('onclick',"favouritejob('"+strval+"','fav',"+ids+")");
                swal(
                'Success',
                'Job Successfully removed',
                'Success'
                )
            }
             }
        } );
    }
  }
 var termTemplate = "<span class='ui-autocomplete-term'>%s</span>";
   $(document).ready(function() {
     $( "#what" ).autocomplete({
      source: function( request, response ) {
        $.ajax( {
          url: "<?php echo url('getjobtitle'); ?>",
          dataType: "jsonp",
          data: {
            term: request.term
          },
          success: function( data ) {
            response( data );
          }
        } );
      },
      minLength: 1
    })
         .autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li>" )
        .append( "<div>" + item.label + "</div>" )
        .appendTo( ul );
    };
    $( "#where" ).autocomplete({
      source: function( request, response ) {
        $.ajax( {
          url: "<?php echo url('getjoblocation'); ?>",
          dataType: "jsonp",
          data: {
            term: request.term
          },
          success: function( data ) {
            response( data );
          }
        } );
      },
      minLength: 1
    })
     .autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li>" )
        .append( "<div>" + item.label + "</div>" )
        .appendTo( ul );
    };
    $( "#where1" ).autocomplete({
      source: function( request, response ) {
        $.ajax( {
          url: "<?php echo url('getjoblocation1'); ?>",
          dataType: "jsonp",
          data: {
            term: request.term
          },
          success: function( data ) {
            response( data );
          }
        } );
      },
      minLength: 1
    })
     .autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li>" )
        .append( "<div>" + item.label + "</div>" )
        .appendTo( ul );
    };
        $("#LOCATION_rbo .more_link").click(function(e){
          $("#LOCATION_rbo .moreLi").show(); 
          $("#LOCATION_rbo .more_link").hide(); 
        });
        $("#rb_Title .more_link").click(function(e){
          $("#rb_Title .moreLi").show(); 
          $("#rb_Title .more_link").hide(); 
        });
        $("#COMPANY_rbo .more_link").click(function(e){
          $("#COMPANY_rbo .moreLi").show(); 
          $("#COMPANY_rbo .more_link").hide(); 
        });
        $("#JOB_TYPE_rbo .more_link").click(function(e){
          $("#JOB_TYPE_rbo .moreLi").show(); 
          $("#JOB_TYPE_rbo .more_link").hide(); 
        });
          $(".title_serach #TITLE_rbo .morespan").click(function(e){
          $(".title_serach #TITLE_rbo .moreLi").css('display','inline-block'); 
          $(".title_serach #TITLE_rbo .morespan").hide(); 
          $(".title_serach #TITLE_rbo .lessspan").show(); 
        });
           $(".title_serach #TITLE_rbo .lessspan").click(function(e){
          $(".title_serach #TITLE_rbo .moreLi").css('display','none'); 
          $(".title_serach #TITLE_rbo .morespan").show(); 
          $(".title_serach #TITLE_rbo .lessspan").hide(); 
        });
    });

function fillq(val) {
    var val=  val.replace(/\-/g," ");
    $("#what").val(val);
    $("#q-box").hide();
}
function filllo(val) {
    var val=  val.replace(/\-/g," ");
    $("#where").val(val);
    $("#lo-box").hide();
}
function filllo1(val) {
    var val=  val.replace(/\-/g," ");
    $("#where1").val(val);
    $("#lo1-box").hide();
}
</script>
<script>
$(document).on('show','.accordion', function (e) {
    $(e.target).prev('.accordion-heading').addClass('accordion-opened');
});
$(document).on('hide','.accordion', function (e) {
    $(this).find('.accordion-heading').not($(e.target)).removeClass('accordion-opened');
});
</script>
@endsection