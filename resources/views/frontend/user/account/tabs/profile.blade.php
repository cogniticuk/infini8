<!-- <table class="table table-striped table-hover">
    <tr>
        <th>{{ trans('labels.frontend.user.profile.avatar') }}</th>
        <td><img src="{{ $logged_in_user->picture }}" class="user-profile-image" /></td>
    </tr>
    <tr>
        <th>{{ trans('labels.frontend.user.profile.name') }}</th>
        <td>{{ $logged_in_user->name }}</td>
    </tr>
    <tr>
        <th>{{ trans('labels.frontend.user.profile.email') }}</th>
        <td>{{ $logged_in_user->email }}</td>
    </tr>
    <tr>
        <th>{{ trans('labels.frontend.user.profile.created_at') }}</th>
        <td>{{ $logged_in_user->created_at }} ({{ $logged_in_user->created_at->diffForHumans() }})</td>
    </tr>
    <tr>
        <th>{{ trans('labels.frontend.user.profile.last_updated') }}</th>
        <td>{{ $logged_in_user->updated_at }} ({{ $logged_in_user->updated_at->diffForHumans() }})</td>
    </tr>
</table> -->



<div class="profile-bxdash">
    <div class="col-md-3">
        <div class="profile-bxdash-img">
             <img src="{{ $logged_in_user->picture }}" class="user-profile-image" />
              <p>{{ $logged_in_user->name }}</p>
        </div>
    </div>

    <div class="col-md-9">
        <div class="profile-bxdash-content">
           
            <p><span class="p-head">Mobile </span> <span class="p-head-dash">:</span><span class="p-result">012-3456-789</span></p>
            <p><span class="p-head">{{ trans('labels.frontend.user.profile.email') }}</span> <span class="p-head-dash">:</span><span class="p-result">aniljmk@gmail.com</span></p>
            <p><span class="p-head">{{ trans('labels.frontend.user.profile.created_at') }}</span> <span class="p-head-dash">:</span><span class="p-result">   ({{ $logged_in_user->created_at->diffForHumans() }})</span></p>
            <p><span class="p-head">{{ $logged_in_user->updated_at }}</span><span class="p-head-dash">:</span> <span class="p-result">     ({{ $logged_in_user->updated_at->diffForHumans() }})</span></p>
            
        </div>
    </div>
</div>  