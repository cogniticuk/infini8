@extends('frontend.layouts.app')

@section('title', app_name() . ' | Find Talent')

@section('content')<!-- <div class="loader" style="display: block;">
                        <div class="ajax-spinner ajax-skeleton"></div>
                    </div> -->
<div class="job_table_view">  
<div class="col-md-12">  

<div class="content-header">
    <h3>
        {{ trans('Find Talent') }}
    </h3>
   </div>
</div>
</div>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<style>
.snip .no-wrap{
  display: none;
}
.company a{
  display: none;
}
.summary{
  display: none;
}
.result-link-source{
  display: none;
}
</style>
  <div class="search_area container_job_list">
    <form method="get" action="" accept-charset="UTF-8" class="searchform" autocomplete="off">
      <!-- <label class="label_st" for='what'> What</label> -->
      <div class="row">
      <span class="inwrap col-sm-4">
      <div class="icon-addon addon-md">
                    <input type="text" name="q" placeholder="Search jobs, companies" value="<?php if(isset($q)) { echo $q; }?> <?php if(isset($_REQUEST['titleLast']) && $_REQUEST['titleLast']!="") { /*echo 'title:'.$_REQUEST['titleLast'];*/ }?>" required="required" class="input_text" id="what" autocomplete="off">
                    <i class="fa fa-suitcase" aria-hidden="true"></i>
                </div>
       
        <div id="q-box" class="suggesstion-box"></div>
      </span>                           
      <!-- <label class="label_st">Where</label> -->
      <span class="inwrap col-sm-4">

      <div class="icon-addon addon-md">
                    <input type="text" name="lo" placeholder="London" value="<?php if(isset($l)) { echo $l; }?>" class="input_text" autocomplete="off" id="where">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                </div>
        
        <div id="lo-box" class="suggesstion-box"></div>
        <!-- <input type="hidden" name="sr" placeholder="What" value="directhire" required="required" class="input_text" id="sr"> -->
      </span>
      <span class="inwrapBorder col-sm-2" style="width:auto;padding-right:0;">
        <span class="inwrapBorderTop"> <input type="submit"  value="Search" class="input_submit" ></span>
      </span>
      <div class="accordion-heading  col-sm-2">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne"><img src="{{ asset('img/search-add.png') }}"></a>
      </div>
    </form>
    </div>
    
    <div class="accordion" id="accordion2">
      <div class="accordion-group">
      
        <div id="collapseOne" class="accordion-body collapse">
          <div class="accordion-inner">
          <form method="get" accept-charset="UTF-8" class="searchform search-form1" autocomplete="off" >
            <div class="col-md-8 col-md-offset-2">
            <div class="ad_search_area adv_jobs">
              
              <h3>Advanced Leads Search</h3>
              <!-- <h4>Find Talents</h4>  -->
             
            <div class="col-md-12 ad_search-prt">
              <div class="form-group">
                <!-- <label class="col-md-5">Keywords</label> -->
                <input id="as_and" name="q" size="35" maxlength="512" autocomplete="off" type="text" placeholder="Keywords" value="<?php if(isset($_REQUEST['query'])) { echo $_REQUEST['query']; }?><?php if(isset($_REQUEST['q'])) { echo $_REQUEST['q']; }?>">
              </div>
              </div>
              <div class="col-md-12 ad_search-prt">
              <div class="form-group">
                <!-- <label class="col-md-5"> Location</label> -->
                <span class="adv_lo col-md-12">
                  <input id="" name="filter_type" size="35" maxlength="512" value="adv" autocomplete="off" type="hidden" > 
                  <input id="where1" name="lo" size="35" maxlength="512" autocomplete="off" type="text" placeholder="Location" value="<?php if(isset($_REQUEST['lo'])) { echo $_REQUEST['lo']; }?>">
                  <div id="lo1-box" class="suggesstion-box"></div> 
                </span>
              </div>
              </div>
               <div class="col-md-12 ad_search-prt">
              <div class="form-group">
                <!-- <label>Title of last job</label> -->
                <input  id="titleLast" name="titleLast" size="35" maxlength="512" autocomplete="off" type="text" placeholder="Job Title" value="<?php if(isset($_REQUEST['titleLast'])) { echo $_REQUEST['titleLast']; }?>">
              </div>
              <div class="form-group">
                <div class="radio-cl">
                <label class="radio-inline">
      <input type="radio" name="jobtitle_type" checked value="Any" <?php if(isset($_REQUEST['jobtitle_type']) && $_REQUEST['jobtitle_type']=="Any") { echo " checked='checked'"; }?>><span>Any</span>
    </label>
    <label class="radio-inline">
      <input type="radio" name="jobtitle_type" value="Latest" <?php if(isset($_REQUEST['jobtitle_type']) && $_REQUEST['jobtitle_type']=="Latest") { echo " checked='checked'"; }?>><span>Latest</span>
    </label>
    </div>
              </div>
              </div>

              <div class="col-md-12 ad_search-prt">
              <div class="form-group">
                <!-- <label class="col-md-5">Company</label> -->
                <input id="companyLast" name="companyLast" size="35" maxlength="512" autocomplete="off" placeholder="Company" type="text" value="<?php if(isset($_REQUEST['companyLast'])) { echo $_REQUEST['companyLast']; }?>">
              </div>
              <div class="form-group">
                <div class="radio-cl">
                <label class="radio-inline">
      <input type="radio" name="Company_type" checked value="Any" <?php if(isset($_REQUEST['Company_type']) && $_REQUEST['Company_type']=="Any") { echo " checked='checked'"; }?>><span>Any</span>
    </label>
    <label class="radio-inline">
      <input type="radio" name="Company_type" value="Latest" <?php if(isset($_REQUEST['Company_type']) && $_REQUEST['Company_type']=="Latest") { echo " checked='checked'"; }?>><span>Latest</span>
    </label>
    </div>
              </div>
              </div>
 
              <!--   <div class="col-md-6 ad_search-prt">
              <div class="form-group">
                <label class="col-md-5"> Show jobs of type</label>
                <select  class="col-md-6" id="jt" name="jt">
                    <option value="all">All job types</option>
                    <option value="temporary" <?php if(isset($_REQUEST['jt']) && $_REQUEST['jt']=="temporary") { echo " selected='selected'"; }?>>Temporary</option>
                    <option value="fulltime" <?php if(isset($_REQUEST['jt']) && $_REQUEST['jt']=="fulltime") { echo " selected='selected'"; }?>>Full-time</option>
                    <option value="parttime" <?php if(isset($_REQUEST['jt']) && $_REQUEST['jt']=="parttime") { echo " selected='selected'"; }?>>Part-time</option>
                    <option value="contract" <?php if(isset($_REQUEST['jt']) && $_REQUEST['jt']=="contract") { echo " selected='selected'"; }?>>Contract</option>
                    <option value="internship" <?php if(isset($_REQUEST['jt']) && $_REQUEST['jt']=="internship") { echo " selected='selected'"; }?>>Internship</option>
                </select>
              </div>
              </div> -->
              <!-- <div class="col-md-6 ad_search-prt">
              <div class="form-group">
                <label class="col-md-5"> From this company</label>
                <input  class="col-md-6" id="as_cmp" name="as_cmp" size="35" maxlength="512"  autocomplete="off" type="text" value="<?php if(isset($_REQUEST['as_cmp'])) { echo $_REQUEST['as_cmp']; }?>">
              </div>
              </div> -->
              <!-- <div class="col-md-6 ad_search-prt">
              <div class="form-group">
                <label class="col-md-5"> Show jobs from</label>
                <select  class="col-md-6" id="st" name="st">
                  <option value="">All web sites</option>
                  <option value="jobsite"<?php if(isset($_REQUEST['st']) && $_REQUEST['st']=="jobsite") { echo " selected='selected'"; }?>>Job boards only</option>
                  <option value="employer"<?php if(isset($_REQUEST['st']) && $_REQUEST['st']=="employer") { echo " selected='selected'"; }?>>Employer web sites only</option>
                </select>
              </div>
              </div> -->
             <!--  <div class="col-md-6 ad_search-prt">
              <div class="form-group col-md-12">
             <label for="norecruiters">Exclude staffing agencies</label> &nbsp;
                <input id="norecruiters" name="sr" value="directhire" type="checkbox" <?php if(isset($_REQUEST['sr']) && $_REQUEST['sr']=="directhire") { echo " checked='checked'"; }?>>
              </div>
              </div> -->
           
              <!-- <div class="col-md-6 ad_search-prt">
              <div class="form-group">
                <label class="col-md-5"> Age - Jobs published</label>
                <select  class="col-md-6" id="fromage" name="fromage">
                  <option value="any" <?php if(isset($_REQUEST['fromage']) && $_REQUEST['fromage']=="any") { echo " selected='selected'"; }?>>anytime</option>
                  <option value="15" <?php if(isset($_REQUEST['fromage']) && $_REQUEST['fromage']=="15") { echo " selected='selected'"; }?>>within 15 days</option>
                  <option value="7" <?php if(isset($_REQUEST['fromage']) && $_REQUEST['fromage']=="7") { echo " selected='selected'"; }?>>within 7 days</option>
                  <option value="3" <?php if(isset($_REQUEST['fromage']) && $_REQUEST['fromage']=="3") { echo " selected='selected'"; }?>>within 3 days</option>
                  <option value="1" <?php if(isset($_REQUEST['fromage']) && $_REQUEST['fromage']=="1") { echo " selected='selected'"; }?>>since yesterday</option>
                  <option value="last" <?php if(isset($_REQUEST['fromage']) && $_REQUEST['fromage']=="last") { echo " selected='selected'"; }?>>since my last visit</option>
                </select>
              </div>
              </div> -->
                <div class="ad-right-button search-leads-btn">
                    <span class="inwrapBorder" style="width:100%;padding-right:0;">
                    <span class="inwrapBorderTop"> <input value="Search" class="input_submit" type="submit"></span>
                    </span>
                </div>
               <!--  <div class="button-ad-sea"><a class="btn pull-left"> Save filter <i class="fa fa-angle-double-right" aria-hidden="true"></i></a> <a class="btn pull-right"> <i class="fa fa-times" aria-hidden="true"></i> Reset filter </a></div> -->
            </div>
            </div>
             </form>
          </div>
        </div>
      </div>
    </div>
  </div>

<div class="job_table_view resume-view-sec">  
<div class="col-md-12">  
 <div class="box box-success">
  <div class="box-header with-border">
 
   </div>
  <div class="box-body">
   <div class="table-responsive">    
    </div>
   </div>
    <div class="animation_image" style="display:none" align="center"><img src="{{ asset('img/ajax-loader.gif') }}"></div>
  </div>
 </div>
 </div>
<input type="hidden" name ="gropcount" id="gropcount" value="">
<script>
 
  function nouser()
  {
    swal(
  'Oops...',
  'Please logged in to favourite job',
  'error'
)
  }  
 var termTemplate = "<span class='ui-autocomplete-term'>%s</span>";
   $(document).ready(function() {    
       
     $( "#what" ).autocomplete({
      source: function( request, response ) {
        $.ajax( {
          url: "<?php echo url('getsal_jobtitle'); ?>",
          dataType: "jsonp",
          data: {
            term: request.term
          },
          success: function( data ) {
            response( data );
          }
        } );
      },
      minLength: 1/*,
      select: function( event, ui ) {
        log( "Selected: " + ui.item.value + " aka " + ui.item.id );
      }*/
    })
         .autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li>" )
        .append( "<div><a href='"+item.link+"'>" + item.label + "</a></div>" )
        .appendTo( ul );
    }; 
        
    });

function fillq(val) {
  var val=  val.replace(/\-/g," ");
$("#what").val(val);
$("#q-box").hide();
}
function filllo(val) {
     var val=  val.replace(/\-/g," ");
$("#where").val(val);
$("#lo-box").hide();
}
function filllo1(val) {
     var val=  val.replace(/\-/g," ");
$("#where1").val(val);
$("#lo1-box").hide();
}


</script>
   <script>
    
    $(document).on('show','.accordion', function (e) {

         $(e.target).prev('.accordion-heading').addClass('accordion-opened');
    });
    
    $(document).on('hide','.accordion', function (e) {
        $(this).find('.accordion-heading').not($(e.target)).removeClass('accordion-opened');
       
    });
    </script>
@endsection
