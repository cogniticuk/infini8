@extends('frontend.layouts.app')

@section('content')
    <div class="dashboard-infini">

    <div class="dashboard-header">
        <h4>Welcome to Dashboard</h4>
    </div>
            @if(isset($items) && count($items)>0)
                @foreach($items as $k=>$mval)
            <div class="dashboard-infini-inner">
                @foreach($mval as $ks=>$sval)
                <a href="{{url('dashboard/').$sval['category_url'].'/'.$sval['category_name']}}"> <!-- category_url -->
                    <div class="col-md-3 job-category-links">
                        <div class="dash-box-sec"> 
						 <p>{{$sval['category_name']}}</p>
						  <h3>{{$sval['job_postings_number']}}                            
                                @if($sval['cat_ch_positive']!="")
                                   <span class="cat_postive_up">{{$sval['cat_ch_positive']}}
                                @elseif($sval['cat_ch_negative']!="")
                                    <span class="cat_negative_down">{{$sval['cat_ch_negative']}}
                                @else
                                    <span class="catnumbes">{{'----'}}
                                @endif
                            </span>
							</h3>
                        </div>
                    </div>
                </a>
                @endforeach
            </div>
            @endforeach
            @endif
    </div>
@endsection
