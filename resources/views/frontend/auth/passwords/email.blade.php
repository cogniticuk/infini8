@extends('frontend.layouts.app_login')

@section('title', app_name() . ' | Reset Password')

@section('content')
<style>
    .panel-default > .panel-heading {
    background: none;
    color: #000;
}
.form-horizontal .control-label {
    text-align: left;
    margin-bottom: 6px;
}
</style>
    <div class="logo-login">
    <a href="{!! url('') !!}">
            <img src="{!! url('img/logo.png') !!}">
    </a>

</div>
        <div class="login-sec">

            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <div class="panel panel-default">

                <div class="panel-heading">{{ trans('labels.frontend.passwords.reset_password_box_title') }}</div>

                <div class="panel-body">

                    {{ Form::open(['route' => 'frontend.auth.password.email.post', 'class' => 'form-horizontal']) }}

                    <div class="form-group">
                        {{ Form::label('email', trans('validation.attributes.frontend.email'), ['class' => 'col-md-12 control-label']) }}
                        <div class="col-md-12">
                            {{ Form::email('email', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.frontend.email')]) }}
                        </div><!--col-md-6-->
                    </div><!--form-group-->


<div class="form-group">
                        <div class="button-form">
                            {{ Form::submit(trans('labels.frontend.passwords.send_password_reset_link_button'), ['class' => 'btn btn-primary', 'style' => 'margin-right:15px']) }}
                          
                        </div><!--col-md-6-->
                    </div><!--form-group-->
                   

                    {{ Form::close() }}

                </div><!-- panel body -->

            </div><!-- panel -->

        </div><!-- col-md-8 -->
@endsection