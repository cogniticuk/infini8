@extends('frontend.layouts.app_login')

@section('content')

<div class="logo-login">
    <a href="{!! url('') !!}">
            <img src="{!! url('img/logo.png') !!}">
    </a>

</div>
        <div class="login-sec">

            <div class="panel panel-default">
               <!--  <div class="panel-heading">{{ trans('labels.frontend.auth.login_box_title') }}</div> -->

                <div class="panel-body">

                    {{ Form::open(['route' => 'frontend.auth.login.post', 'class' => 'form-horizontal']) }}


                   <!--  <div class="img-sec">
                        
                        <img src="{!! url('img/user.png') !!}">
                    </div> -->

                    <div class="login-top-head">
                        <h4>Sign in</h4>
                    </div>

                    <div class="form-group">
                       <!--  {{ Form::label('email', trans('validation.attributes.frontend.email'), ['class' => 'col-md-4 control-label']) }} -->
                        <div class="form-input">
                            {{ Form::email('email', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.frontend.email')]) }}
                        </div><!--col-md-6-->
                    </div><!--form-group-->

                     <div class="form-group">
                        <div class="form-input">
                            {{ Form::password('password', ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.password')]) }}
                        </div>
                    </div> 

                     <div class="form-group">
                        <div class="form-input">
                            <div class="checkbox">
                                <label>
                                    {{ Form::checkbox('remember') }} {{ trans('labels.frontend.auth.remember_me') }}
                                </label>
                            </div>
                        </div>
                    </div> 

                    <div class="form-group">
                        <div class="button-form">
                            {{ Form::submit(trans('labels.frontend.auth.login_button'), ['class' => 'btn btn-primary', 'style' => 'margin-right:15px']) }}
                          
                        </div><!--col-md-6-->
                    </div><!--form-group-->

                    <div class="form-group">
                    <div class="form-input">
                    <p>{{ link_to_route('frontend.auth.password.reset', trans('labels.frontend.passwords.forgot_password')) }} </p>
                         
                    </div>
                    </div>

                    {{ Form::close() }}

                    <div class="row text-center">
                        {!! $socialite_links !!}
                    </div>

                    </div>
                </div><!-- panel body -->

            </div><!-- panel -->

        </div><!-- col-md-8 -->


@endsection