<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title', app_name())</title>

        <!-- Meta -->
        <meta name="description" content="@yield('meta_description', 'Residential Management System')">
        <meta name="author" content="@yield('meta_author', 'WDP Technologies Pvt. Ltd.')">
        @yield('meta')

        <!-- Styles -->
        @yield('before-styles')

        <!-- Check if the language is set to RTL, so apply the RTL layouts -->
        <!-- Otherwise apply the normal LTR layouts -->
        @langRTL
            {{ Html::style(getRtlCss(mix('css/frontend.css'))) }}
        @else
            {{ Html::style(mix('css/frontend.css')) }}
        @endif

        @yield('after-styles')
        {{ Html::style('css/bootstrap.css?v=3') }}
{{ Html::style('css/style.css?v=3') }}
{{ Html::style('css/scrollToTop.css') }}

<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet"> 

  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <!-- Scripts -->
        <script>
            window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>
        </script>
    </head>
    <body id="app-layout">
        <div id="app" style="background: #222d32">
            @include('includes.partials.logged-in-as')
            @include('frontend.includes.nav')
            @include('frontend.includes.sidebar')
<div class="wrap-fluid">
<div class="">@yield('page-header')
                @include('includes.partials.messages')
                
                @yield('content')
        <a href="#" id="scrollToTop" class="scrollToTop scrollToTop_cycle scrollToTop_fade">Scroll To Top</a>
<!-- <script type="text/javascript">
        $(document).ready(function($) {
            $('body').scrollToTop({skin: 'cycle', easing: 'easeInOutElastic'});
        });
</script> -->
<script>
// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("scrollToTop").style.display = "block";
    } else {
        document.getElementById("scrollToTop").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}
</script>
</div>
        </div><!--#app-->
        </div>
{{ Html::script('js/bootstrap.min.js') }}
<!-- {{ Html::script('js/sliding-menu.js') }}
        Scripts -->
        @yield('before-scripts')
        
        @yield('after-scripts')

        @include('includes.partials.ga')
		
		<script>
			function hideclass(){
				$("body").addClass("header_small_logo");
				$("#toggle").attr("onclick",'showclass()');
			}
			function showclass(){
				$("body").removeClass("header_small_logo");
				$("#toggle").attr("onclick",'hideclass()');
			}		 
		</script>
    </body>
</html>