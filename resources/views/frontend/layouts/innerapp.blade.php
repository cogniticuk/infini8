<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title', app_name())</title>

        <!-- Meta -->
        <meta name="description" content="@yield('meta_description', 'Residential Management System')">
        <meta name="author" content="@yield('meta_author', 'WDP Technologies Pvt. Ltd.')">
        @yield('meta')

        <!-- Styles -->
        @yield('before-styles')

        <!-- Check if the language is set to RTL, so apply the RTL layouts -->
        <!-- Otherwise apply the normal LTR layouts -->
        @langRTL
            {{ Html::style(getRtlCss(mix('css/frontend.css'))) }}
        @else
            {{ Html::style(mix('css/frontend.css')) }}
        @endif

        @yield('after-styles')
         <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

        {{ Html::style('css/bootstrap.css?v=3') }}
        {{ Html::style('css/style.css?v=3') }}

  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>  

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <!-- Scripts -->
        <script>
            window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>
        </script>
    </head>
    <body id="app-layout">
        <div id="app">
            @include('includes.partials.logged-in-as')
            @include('frontend.includes.nav')

            @include('frontend.includes.sidebar')

            
                @include('includes.partials.messages')
                @yield('content')

        </div><!--#app-->
{{ Html::script('js/bootstrap.min.js') }}

        <!-- Scripts -->
        @yield('before-scripts')
        
        @yield('after-scripts')

        @include('includes.partials.ga')
    </body>
</html>