

<header class="main-header">

    <a href="{!! url('/dashboard') !!}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">
           <img src="{!! url('img/small_logo.png') !!}">
        </span>

        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">
            <img src="{!! url('img/logo.png') !!}">
        </span>
    </a>

    <nav class="navbar navbar-static-top" role="navigation">
        <a id="toggle" href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button" onclick="hideclass();">
            <i class="fa fa-bars" aria-hidden="true"></i>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

                               <!--      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <i class="fa fa-language"></i> Language <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                                                <li><a href="http://localhost/indeed/www/public/lang/ar">Arabic</a></li>
                                                                <li><a href="http://localhost/indeed/www/public/lang/zh">Chinese Simplified</a></li>
                                                                <li><a href="http://localhost/indeed/www/public/lang/zh-TW">Chinese Traditional</a></li>
                                                                <li><a href="http://localhost/indeed/www/public/lang/da">Danish</a></li>
                                                                <li><a href="http://localhost/indeed/www/public/lang/de">German</a></li>
                                                                <li><a href="http://localhost/indeed/www/public/lang/el">Greek</a></li>
                                                                                        <li><a href="http://localhost/indeed/www/public/lang/es">Spanish</a></li>
                                                                <li><a href="http://localhost/indeed/www/public/lang/fr">French</a></li>
                                                                <li><a href="http://localhost/indeed/www/public/lang/id">Indonesian</a></li>
                                                                <li><a href="http://localhost/indeed/www/public/lang/it">Italian</a></li>
                                                                <li><a href="http://localhost/indeed/www/public/lang/ja">Japanese</a></li>
                                                                <li><a href="http://localhost/indeed/www/public/lang/nl">Dutch</a></li>
                                                                <li><a href="http://localhost/indeed/www/public/lang/pt_BR">Brazilian Portuguese</a></li>
                                                                <li><a href="http://localhost/indeed/www/public/lang/ru">Russian</a></li>
                                                                <li><a href="http://localhost/indeed/www/public/lang/sv">Swedish</a></li>
                                                                <li><a href="http://localhost/indeed/www/public/lang/th">Thai</a></li>
                                                                <li><a href="http://localhost/indeed/www/public/lang/tr">Turkish</a></li>
                        </ ul>                   </li>--> 
                
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <img src="http://www.gravatar.com/avatar/8f5e62f881db7cc7e90f4ec37bc6e60e.jpg?s=80&amp;d=mm&amp;r=g" class="user-image" alt="User Avatar">
                        <span class="hidden-xs">{{ $logged_in_user->name }}</span>
                    </a>

                    <ul class="dropdown-menu">
                        <li class="user-header">
                            <img src="http://www.gravatar.com/avatar/8f5e62f881db7cc7e90f4ec37bc6e60e.jpg?s=80&amp;d=mm&amp;r=g" class="img-circle" alt="User Avatar">
                            <p>
                                {{ $logged_in_user->name }}
                                <small>Member since {{ date('m/d/Y',strtotime($logged_in_user->created_at)) }}</small>
                            </p>
                        </li>

                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{url('account')}}" class="btn btn-default btn-flat">
                                    <i class="fa fa-home"></i>
                                    My Account
                                </a>
                            </div>
                            <div class="pull-right">
                         
                                <a href="{{url('logout')}}" class="btn btn-danger btn-flat">
                                    <i class="fa fa-sign-out"></i>
                                    Logout
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-custom-menu -->
    </nav>
</header>



        <!-- <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#frontend-navbar-collapse">
                <span class="sr-only">{{ trans('labels.general.toggle_navigation') }}</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="{!! url('') !!}"><img src="{!! url('img/logo.png') !!}"></a>
            
            
        </div>

        <div class="collapse navbar-collapse" id="frontend-navbar-collapse">
            
            <ul class="nav navbar-nav navbar-right">
                <li>{{ link_to_route('frontend.myjob', trans('Jobs'), [], ['class' => active_class(Active::checkRoute('myjob')) ]) }}</li>
                <li>{{ link_to_route('frontend.getresumes', trans('Resumes'), [], ['class' => active_class(Active::checkRoute('getresumes')) ]) }}</li>
                @if (config('locale.status') && count(config('locale.languages')) > 1)
                     <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ trans('menus.language-picker.language') }}
                            <span class="caret"></span>
                        </a>

                        @include('includes.partials.lang')
                    </li> 
                @endif

                @if ($logged_in_user)
                    <li>{{ link_to_route('frontend.dashboard', trans('navs.frontend.dashboard'), [], ['class' => active_class(Active::checkRoute('frontend.dashboard')) ]) }}</li>
                @endif

                @if (! $logged_in_user)
                    <li>{{ link_to_route('frontend.auth.login', trans('navs.frontend.login'), [], ['class' => active_class(Active::checkRoute('frontend.auth.login')) ]) }}</li>

                    @if (config('access.users.registration'))
                        <li>{{ link_to_route('frontend.auth.register', trans('navs.frontend.register'), [], ['class' => active_class(Active::checkRoute('frontend.auth.register')) ]) }}</li>
                    @endif
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ $logged_in_user->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            @permission('view-backend')
                                <li>{{ link_to_route('admin.dashboard', trans('navs.frontend.user.administration')) }}</li>
                            @endauth
                            <li>{{ link_to_route('frontend.user.account', trans('navs.frontend.user.account'), [], ['class' => active_class(Active::checkRoute('frontend.user.account')) ]) }}</li>
                            <li>{{ link_to_route('frontend.auth.logout', trans('navs.general.logout')) }}</li>
                        </ul>
                    </li>
                @endif

                <li>{{ link_to_route('frontend.contact', trans('navs.frontend.contact'), [], ['class' => active_class(Active::checkRoute('frontend.contact')) ]) }}</li>
            </ul>
        </div> -->
