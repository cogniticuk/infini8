<aside class="main-sidebar search_sidebar" id="skin-select"  style="visibility: visible;">
    <section class="sidebar">
        <ul class="sidebar-menu"> <!--id="menu-showhide" class="topnav"-->
		<div class="user-panel">
            <div class="pull-left image">
                <img src="http://www.gravatar.com/avatar/8f5e62f881db7cc7e90f4ec37bc6e60e.jpg?s=80&amp;d=mm&amp;r=g" class="img-circle" alt="User Image">
            </div><!--pull-left-->
            <div class="pull-left info">
                <p>{{ $logged_in_user->name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div><!--pull-left-->
        </div>
		  <form method="GET" action="" accept-charset="UTF-8" class="sidebar-form">
             <div class="input-group">
               <input class="form-control" required="required" placeholder="Search..." name="q" type="text">
                  <span class="input-group-btn">
                    <button type="submit" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                  </span>
              </div>
            </form>
            <li class="{{active_class(Active::checkUriPattern('dashboard'))}}">
                <a href="{{url('dashboard')}}" class="tooltip-tip tooltipster-disable ">
                <i class="fa fa-dashboard"  style="left: -11px;"></i>
                <span style="display: inline-block; float: none;">Dashboard</span>
                </a>
            </li>
            <li class="treeview {{active_class(Active::checkUriPattern('myjob'))}} {{active_class(Active::checkUriPattern('getresumes'))}}">
                <a href="javascript:void(0)" class="tooltip-tip tooltipster-disable">
                <i class="fa fa-gbp"  style="left: -11px;"></i>
                <span style="display: inline-block; float: none;">Opportunities</span>
                <i class="fa fa-angle-down pull-right"></i> 
                </a>
                <ul class="treeview-menu" style="display: none;">

                    <li>
                        <a href="{{url('myjob')}}"  class="tooltip-tip tooltipster-disable">
                        <i class="fa fa-circle-o"  style="left: -11px;"></i>                        
                        <span style="display: inline-block; float: none;">Find Leads</span>
                        </a>
                    </li>
                    <li  class="tooltip-tip tooltipster-disable">
                        <a href="{{url('getresumes')}}"  class="tooltip-tip tooltipster-disable">
                        <i class="fa fa-circle-o"  style="left: -11px;"></i>                       
                        <span style="display: inline-block; float: none;">Find Talent</span>
                        </a>
                    </li>
                </ul> 
            </li>
            <li class="treeview">
                <a href="javascript:void(0)" class="tooltip-tip tooltipster-disable">
                <i class="fa fa-tasks"  style="left: -11px;"></i>
                <span style="display: inline-block; float: none;">Reports</span>
                </a>
            </li>
            <li class="treeview1 {{active_class(Active::checkUriPattern('guided_analysis'))}} {{active_class(Active::checkUriPattern('trends_datails'))}} {{active_class(Active::checkUriPattern('salary_watch'))}}">
                <a href="javascript:void(0)" class="tooltip-tip tooltipster-disable">
                <i class="fa fa-file-text-o"  style="left: -11px;"></i>
                <span style="display: inline-block; float: none;">Market Analysis</span>
                 <i class="fa fa-angle-down pull-right"></i>
                </a>
                 <ul class="treeview-menu1" style="display: none;">
                    <li>
                        <a href="{{url('guided_analysis')}}"  class="tooltip-tip tooltipster-disable">
                        <i class="fa fa-circle-o"  style="left: -11px;"></i>                        
                        <span style="display: inline-block; float: none;">Guided Analysis </span>
                        </a>
                    </li>
                    <li  class="tooltip-tip tooltipster-disable">
                        <a href="{{url('trends/accounting-category-trends/Accounting')}}"  class="tooltip-tip tooltipster-disable">
                        <i class="fa fa-circle-o"  style="left: -11px;"></i>                       
                        <span style="display: inline-block; float: none;">Top Trends</span>
                        </a>
                    </li>
                     <li  class="tooltip-tip tooltipster-disable">
                        <a href="{{url('salary_watch')}}"  class="tooltip-tip tooltipster-disable">
                        <i class="fa fa-circle-o"  style="left: -11px;"></i>                       
                        <span style="display: inline-block; float: none;">Salary Watch </span>
                        </a>
                    </li>                     
                </ul> 
            </li>
        </ul>
    </section>
</aside>


<script>
$(document).ready(function(){
    $(".treeview").click(function(){
        $(".treeview-menu").slideToggle(500);
    });
     $(".treeview1").click(function(){
        $(".treeview-menu1").slideToggle(500);
    });
});
</script>