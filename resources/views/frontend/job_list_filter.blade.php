<?php 
if(count($final)>0)
{
	$locations="";
	if($l!="")
	{
		$locations=$l;
	}
	foreach($final as $key=>$val){
		if(isset($val['title'])){		
			$title = $val['title'];
		}else{
			$title = "" ;
		}
		if(isset($val['companyname']) && $val['companyname']!=""){			
			$companyname = $val['companyname'];
			$companyname1 = $val['companyname'];			  
		}
		else{
			$companyname = ""; 
			$companyname1 = '';
		} 
		?>
		<tr>
			<td>
				<a href='https://www.indeed.co.uk/<?php echo $val['link'] ;?>' target='_blank'>{{$title}}</a>
			</td>
			<td class="companyname-color">
				<a href="<?php echo url('myjob?as_cmp=').str_replace(' ', '-', $companyname1 )."&l=".$locations."&viewtype=v" ?>"  target="_blank">{{ $companyname}}</a>
			</td>
			<td>{{$val['location']}}</td>
			<td>{{$val['jobsource']}}</td>
			<td>
				<?php if(isset($val['postdate']) && $val['postdate']!=""){
					echo $val['postdate'];
				 } 
				?>				
			</td>
			<td>
				<a class="btn btn-xs btn-info" href='https://www.indeed.co.uk/<?php echo $val['link'] ;?>' target='_blank'><img src="{{ asset('img/view.png') }}"></a>
				<a class="btn btn-xs btn-primary" href='https://www.linkedin.com/search/results/people/?keywords={{$companyname1}}&origin=GLOBAL_SEARCH_HEADER' target='_blank'><img src="{{ asset('img/indded.png') }}"></a>
				<a class="btn btn-xs btn-danger" href='https://www.google.com/search?q={{$companyname1}} contact' target='_blank'><img src="{{ asset('img/google.png') }}"></a>
				<a class="btn btn-xs btn-info" href="<?php echo url('getresumes?q=').str_replace(' ', '-', $title )."&l=".$val['location']."&viewtype=s" ?>" target='_blank'><img src="{{ asset('img/users.png') }}"></a>
			<?php 
			if(!Auth::user()) 
			{
				echo "<a class='btn btn-xs btn-warning' href='javascript:void(0);' onclick='nouser();'><img src=".asset('img/plophy.png')." alt='Save' title='Save'></a>";
			}else{
				$checkjobs=checkjob($val['link']);
				if($checkjobs=="true")
				{
					echo "<a class='btn btn-xs btn-warning' id='fav_".$key."'class='fav_un' href='javascript:void(0);' onclick=favouritejob('".base64_encode($val['link'])."','unfav',".$key.")><img src=".asset('img/plophy.png')." alt='Saved' title='Saved'></a>";
				}else{
					echo "<a class='btn btn-xs btn-warning' id='fav_".$key."'class='fav_un' href='javascript:void(0);' onclick=favouritejob('".base64_encode($val['link'])."','fav',".$key.")><img src=".asset('img/plophy.png')." alt='Save' title='Save'></a>";
				}
			}?></td>
		</tr>
		<?php 
	}
}else{
	echo "<div class='re-not-found'>Result could not be found.</div>";
}?>