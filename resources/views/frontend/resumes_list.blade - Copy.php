@extends('frontend.layouts.app')

@section('title', app_name() . ' | Resumes List')

@section('content')
<style>
.snip .no-wrap{
  display: none;
}
.company a{
  display: none;
}
.summary{
  display: none;
}
.result-link-source{
  display: none;
}
.job_list_blk {
  
  min-height: 230px !important;
  
}
</style>
<div class="container">
  <div class="search_area">
  <form method="get" action="" accept-charset="UTF-8" class="searchform" autocomplete="off">
    <!-- <label class="label_st" for='what'> What</label> -->
    <span class="inwrap">
      
       <div class="icon-addon addon-md">
                   <input type="text" name="q" placeholder="What" value="<?php if(isset($q)) { echo $q; }?>" required="required" class="input_text" id="what" autocomplete="off">
                    <i class="fa fa-suitcase" aria-hidden="true"></i>
                </div>
      <div id="q-box" class="suggesstion-box"></div>
    </span>                           
    <!-- <label class="label_st">Where</label> -->
    <span class="inwrap">
      
      <div class="icon-addon addon-md">
                    <input type="text" name="lo" placeholder="Where" value="<?php if(isset($l)) { echo $l; }?>" class="input_text" autocomplete="off" id="where">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                </div>
      <div id="lo-box" class="suggesstion-box"></div>
      <!-- <input type="hidden" name="sr" placeholder="What" value="directhire" required="required" class="input_text" id="sr"> -->
    </span>
    <span class="inwrapBorder" style="width:auto;padding-right:0;">
      <span class="inwrapBorderTop"> <input type="submit"  value="Find Resumes" class="input_submit" ></span>
    </span>
  </form>
  <!-- <div class="title_serach">
  <?php echo $rb_title; //#accordion2?>
  </div> -->
    <div class="accordion" id="accordion2">
        <div class="accordion-group">
          <div class="accordion-heading">
            <a class="accordion-toggle" data-toggle="collapse" data-parent="" href="">Advanced Search</a>
          </div>
          <div id="collapseOne" class="accordion-body collapse">
            <div class="accordion-inner">
              <div class="ad_search_area adv_jobs">
              <form method="get" accept-charset="UTF-8" class="searchform" autocomplete="off" >
                <h3>Advanced Resume Search</h3>
                <h4>Find resume</h4> 
                <div class="form-group">
                  <label class="col-md-4"> With none of these words</label>
                  <input  class="col-md-6" id="as_not" name="as_not" size="35" maxlength="512" autocomplete="off" type="text" value="<?php if(isset($_REQUEST['as_not'])) { echo $_REQUEST['as_not']; }?>">
                </div>
                <div class="form-group">
                  <label class="col-md-4"> From this company</label>
                  <input  class="col-md-6" id="as_cmp" name="as_cmp" size="35" maxlength="512"  autocomplete="off" type="text" value="<?php if(isset($_REQUEST['as_cmp'])) { echo $_REQUEST['as_cmp']; }?>">
                </div>
                <div class="form-group">
                  <label class="col-md-4"> Show jobs from</label>
                  <select  class="col-md-6" id="st" name="st">
                    <option value="">All web sites</option>
                    <option value="jobsite"<?php if(isset($_REQUEST['st']) && $_REQUEST['st']=="jobsite") { echo " selected='selected'"; }?>>Job boards only</option>
                    <option value="employer"<?php if(isset($_REQUEST['st']) && $_REQUEST['st']=="employer") { echo " selected='selected'"; }?>>Employer web sites only</option>
                  </select>
                </div>
                <div class="form-group">
                  <label class="col-md-4"> </label>
                  <input id="norecruiters" name="sr" value="directhire" type="checkbox" <?php if(isset($_REQUEST['sr']) && $_REQUEST['sr']=="directhire") { echo " checked='checked'"; }?>>&nbsp;<label for="norecruiters">Exclude staffing agencies</label>
                </div>
                <div class="form-group">
                  <label class="col-md-4"> Location</label>
                  <span class="adv_lo col-md-6 ">
                  <input id="" name="filter_type" size="35" maxlength="512" value="adv" autocomplete="off" type="hidden" >
                  <input id="where1" name="lo" size="35" maxlength="512" autocomplete="off" type="text" value="<?php if(isset($_REQUEST['lo'])) { echo $_REQUEST['lo']; }?>">
                  <div id="lo1-box" class="suggesstion-box"></div>
                  </span>
                </div>
                <div class="form-group">
                  <label class="col-md-4"> Age - Jobs published</label>
                  <select  class="col-md-6" id="fromage" name="fromage">
                    <option value="any" <?php if(isset($_REQUEST['fromage']) && $_REQUEST['fromage']=="any") { echo " selected='selected'"; }?>>anytime</option>
                    <option value="15" <?php if(isset($_REQUEST['fromage']) && $_REQUEST['fromage']=="15") { echo " selected='selected'"; }?>>within 15 days</option>
                    <option value="7" <?php if(isset($_REQUEST['fromage']) && $_REQUEST['fromage']=="7") { echo " selected='selected'"; }?>>within 7 days</option>
                    <option value="3" <?php if(isset($_REQUEST['fromage']) && $_REQUEST['fromage']=="3") { echo " selected='selected'"; }?>>within 3 days</option>
                    <option value="1" <?php if(isset($_REQUEST['fromage']) && $_REQUEST['fromage']=="1") { echo " selected='selected'"; }?>>since yesterday</option>
                    <option value="last" <?php if(isset($_REQUEST['fromage']) && $_REQUEST['fromage']=="last") { echo " selected='selected'"; }?>>since my last visit</option>
                  </select>
                </div>
                <div class="col-md-12">
                  <div class="col-md-3"></div>
                  <div class="col-md-9">
                    <span class="inwrapBorder" style="width:auto;padding-right:0;">
                      <span class="inwrapBorderTop"> <input value="Find Resumes" class="input_submit" type="submit"></span>
                    </span>
                  </div>
                </div>
              </form>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
<div class="job_list_block">
  <div class="container-fluid">
    <div class="panel panel-default">
      <!-- <div class="panel-heading">{{ trans('Resumes list') }}</div>    -->                
      <div class="panel-body">                   
        <div class="mainsection">
        <?php if(trim($filters)!=""){?>
        <div class="col-md-3">
          <div class="filters" id="refineresults">                            
            <?php  echo $filters;?>                            
          </div>
        </div>
        <?php  }
        ?> 
          <div class="col-md-9">
            <div class="middlesec">
              <?php 
              if(count($final)>0)
              {
                foreach($final as $val){
                  echo "<div class='col-md-4'><div class='job_list_blk'>".$val['summary']."<div class='s_link'>
                <a href='javascript:void(0);'>View</a> <a href='javascript:void(0);'>Linkedin</a> <a href='javascript:void(0);'>Google</a>  <a href='javascript:void(0);'>Save</a>
                </div></div></div>";
                }?>
                <br>
                <div class="pagination" onmousedown="pclk(event);">
                  <?php if($pagination!=""){
                    echo $pagination;
                  }
                  ?>
                </div>
                <?php
              }else{
                echo "<div class='re-not-found'>Result could not be found.</div>";
              }?>
            </div>
          </div>
        </div>
      </div><!-- panel body -->
    </div><!-- panel -->
  </div><!-- col-md-8 -->
</div><!-- row -->     
<script>
 var termTemplate = "<span class='ui-autocomplete-term'>%s</span>";
   $(document).ready(function() {
         /*$("#what").keyup(function(e){
            if(this.value!=""){
                $.ajax({
                  type:"get",
                  url: "<?php echo url('getjobtitle'); ?>?q="+this.value,
                  datatype:"json",
                  success: function(response) 
                  {  
                    $("#q-box").show();
                    $("#q-box").html(response);
                    $("#what").css("background","#FFF");
                  }
                });
            }
        });*/
     $( "#what" ).autocomplete({
      source: function( request, response ) {
        $.ajax( {
          url: "<?php echo url('getjobtitle'); ?>",
          dataType: "jsonp",
          data: {
            term: request.term
          },
          success: function( data ) {
            response( data );
          }
        } );
      },
      minLength: 1/*,
      select: function( event, ui ) {
        log( "Selected: " + ui.item.value + " aka " + ui.item.id );
      }*/
    })
         .autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li>" )
        .append( "<div>" + item.label + "</div>" )
        .appendTo( ul );
    };
    $( "#where" ).autocomplete({
      source: function( request, response ) {
        $.ajax( {
          url: "<?php echo url('getjoblocation'); ?>",
          dataType: "jsonp",
          data: {
            term: request.term
          },
          success: function( data ) {
            response( data );
          }
        } );
      },
      minLength: 1/*,
      select: function( event, ui ) {
        log( "Selected: " + ui.item.value + " aka " + ui.item.id );
      }*/
    })
     .autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li>" )
        .append( "<div>" + item.label + "</div>" )
        .appendTo( ul );
    };
    $( "#where1" ).autocomplete({
      source: function( request, response ) {
        $.ajax( {
          url: "<?php echo url('getjoblocation1'); ?>",
          dataType: "jsonp",
          data: {
            term: request.term
          },
          success: function( data ) {
            response( data );
          }
        } );
      },
      minLength: 1/*,
      select: function( event, ui ) {
        log( "Selected: " + ui.item.value + " aka " + ui.item.id );
      }*/
    })
     .autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li>" )
        .append( "<div>" + item.label + "</div>" )
        .appendTo( ul );
    };
         /*$("#where").keyup(function(e){
            if(this.value!=""){
                $.ajax({
                  type:"get",
                  url: "<?php echo url('getjoblocation'); ?>?q="+this.value,
                  datatype:"json",
                  success: function(response) 
                  {  
                    $("#lo-box").show();
                    $("#lo-box").html(response);
                    $("#where").css("background","#FFF");
                  }
                });
            }
        });
          $("#where1").keyup(function(e){
            if(this.value!=""){
                $.ajax({
                  type:"get",
                  url: "<?php echo url('getjoblocation1'); ?>?q="+this.value,
                  datatype:"json",
                  success: function(response) 
                  {  
                    $("#lo1-box").show();
                    $("#lo1-box").html(response);
                    $("#where1").css("background","#FFF");
                  }
                });
            }
        });*/
        $("#LOCATION_rbo .more_link").click(function(e){
          $("#LOCATION_rbo .moreLi").show(); 
          $("#LOCATION_rbo .more_link").hide(); 
        });
        $("#rb_Title .more_link").click(function(e){
          $("#rb_Title .moreLi").show(); 
          $("#rb_Title .more_link").hide(); 
        });
        $("#COMPANY_rbo .more_link").click(function(e){
          $("#COMPANY_rbo .moreLi").show(); 
          $("#COMPANY_rbo .more_link").hide(); 
        });
        $("#JOB_TYPE_rbo .more_link").click(function(e){
          $("#JOB_TYPE_rbo .moreLi").show(); 
          $("#JOB_TYPE_rbo .more_link").hide(); 
        });
        $(".title_serach #TITLE_rbo .more_link").click(function(e){
          $(".title_serach #TITLE_rbo .moreLi").css('display','inline-block'); 
          $(".title_serach #TITLE_rbo .more_link").hide(); 
        });
    });
function fillq(val) {
  var val=  val.replace(/\-/g," ");
$("#what").val(val);
$("#q-box").hide();
}
function filllo(val) {
     var val=  val.replace(/\-/g," ");
$("#where").val(val);
$("#lo-box").hide();
}
function filllo1(val) {
     var val=  val.replace(/\-/g," ");
$("#where1").val(val);
$("#lo1-box").hide();
}


</script>
   <script>
    
    $(document).on('show','.accordion', function (e) {

         $(e.target).prev('.accordion-heading').addClass('accordion-opened');
    });
    
    $(document).on('hide','.accordion', function (e) {
        $(this).find('.accordion-heading').not($(e.target)).removeClass('accordion-opened');
       
    });
    </script>
@endsection