@extends('frontend.layouts.app')

@section('content')
    <div class="dashboard-infini">

    <div class="dashboard-header acc_heading">
        <h4>{{$ctaname}}</h4>
		<div class="container">
		 <div class="col-md-12 col-sm-12 Topblock">
		  <div class="account_box">
		   <div class="col-md-6 col-sm-6">
		    <h3>Job postings</h3>
			<p>Average job postings this month:</p>
			<h2>@if(isset($items[0]['job_postings_number']) && $items[0]['job_postings_number']!=""){{$items[0]['job_postings_number']}}@else {{-----}}@endif</h2>
		   </div>
		   <div class="col-md-6 col-sm-6">
		    <h3>Change in job market share in percentage points</h3>
			<p>Compared to last month:</p>
			
           <h5 class="cat_postive_up">
           	@if($items[0]['cat_ch_positive']!="")
           	{{$items[0]['cat_ch_positive']}}
	        @elseif($items[0]['cat_ch_negative']!="")
	            <h5 class="cat_negative_down">{{$items[0]['cat_ch_negative']}}
	        @else
	            <h5 class="catnumbes">{{'----'}}
	        @endif
              </h5>
			<p>Compared to last year:</p>
			<h5 class="cat_postive_up">
           	@if($items[0]['cat_ch_positivey']!="")
           	{{$items[0]['cat_ch_positivey']}}
	        @elseif($items[0]['cat_ch_negativey']!="")
	            <h5 class="cat_negative_down">{{$items[0]['cat_ch_negativey']}}
	        @else
	            <h5 class="catnumbes">{{'----'}}
	        @endif
              </h5>
		   </div>
		  </div>
		 </div>
		 <div class="col-md-4 col-sm-4 Topblock">
		  <div class="Top-List">      
			<h2 class="top-header">Top job titles per click</h2>
			@if(isset($perclickdata[1]) && $perclickdata[1]!="")
				@foreach($perclickdata[1] as $k=>$v)
				<div class="job_title">{{$v}}<span>{{$perclickdata[2][$k]}}</span></div>
				@endforeach
			@endif
		  </div>
		 </div>
          <div class="col-md-4 col-sm-4 Topblock">
          <div class="Top-List">      
            <h2 class="top-header">Top search terms per click</h2>
        	@if(isset($perclickdata[3]) && $perclickdata[3]!="")
				@foreach($perclickdata[3] as $k=>$v)
					<div class="job_title">{{$v}}<span>{{$perclickdata[4][$k]}}</span></div>
				@endforeach
			@endif
          </div>
         </div>
          <div class="col-md-4 col-sm-4 Topblock">
          <div class="Top-List">      
            <h2 class="top-header">Top job locations per click</h2>
        	@if(isset($perclickdata[5]) && $perclickdata[5]!="")
				@foreach($perclickdata[5] as $k=>$v)
					<div class="job_title">{{$v}}<span>{{$perclickdata[6][$k]}}</span></div>
				@endforeach
			@endif
          </div>
         </div>
		</div>
    </div>
            
    </div>
@endsection
