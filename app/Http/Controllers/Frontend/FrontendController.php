<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use simple_html_dom;
use Request;
use Auth;
use DB;
use Redirect;
 include(public_path().'/simple_html_dom.php');
/**
 * Class FrontendController.
 */
class FrontendController extends Controller
{
		/**
		 * @return \Illuminate\View\View
		 */
		public function index()
		{
			//return Redirect::to('login');exit;
				return view('frontend.index');
		}

		/**
		 * @return \Illuminate\View\View
		 */
		public function macros()
		{
				return view('frontend.macros');
		}
		public function getjobs()
		{
				$siteurls=Request::url();
				$html = new simple_html_dom();
				$final=array();
				$umain=explode('?', $_SERVER['REQUEST_URI']);
				$page_limit=env('PAGE_LIMIT','10');
				if(isset($_REQUEST['filter_type']) && $_REQUEST['filter_type']=="adv"){
					$st_u=str_replace("filter_type=adv", '', $umain[1]);
					$st_u=str_replace("lo=", 'l=', $st_u);
					$url="https://www.indeed.co.uk/jobs?".$st_u."&sort=date&limit=".$page_limit;
				}
				else if(count($umain)>0 && isset($umain[1]) && $umain[1]!=""){
						if(count($umain)==3 ){
								$url="https://www.indeed.co.uk/jobs?".$umain[2]."&sort=date&limit=".$page_limit;  
						}
						else{
							if(isset($_REQUEST) && $_REQUEST!="" && ((isset($_REQUEST['q']) && $_REQUEST['q']!="") || (isset($_REQUEST['lo']) && $_REQUEST['lo']!=""  ) || (isset($_REQUEST['as_cmp']) && $_REQUEST['as_cmp']!=""  ))){
									$url="https://www.indeed.co.uk/jobs?".$umain[1]."&sort=date&limit=".$page_limit; 
									if(isset($_REQUEST['as_cmp']) && $_REQUEST['as_cmp']!=""  )
									{
										$url="https://www.indeed.co.uk/jobs?as_cmp=".$_REQUEST['as_cmp']."&l=".trim($_REQUEST['l'])."&sort=date&limit=".$page_limit;
									}
							}
							else{                      
									$url="https://www.indeed.co.uk/".$umain[1]; 
							}
							$url=str_replace("&lo", '&l', $url);
						}
				}
				else if(isset($_REQUEST) && $_REQUEST!="" && ((isset($_REQUEST['q']) && $_REQUEST['q']!="") || (isset($_REQUEST['lo']) && $_REQUEST['lo']!=""  ) || (isset($_REQUEST['as_cmp']) && $_REQUEST['as_cmp']!=""  ))){

					$url="https://www.indeed.co.uk/jobs?q=".$_REQUEST['q']."&l=".trim($_REQUEST['lo'])."&sort=date&limit=".$page_limit;
					if(isset($_REQUEST['as_cmp']) && $_REQUEST['as_cmp']!=""  )
					{
						$url="https://www.indeed.co.uk/jobs?as_cmp=".$_REQUEST['as_cmp']."&l=".trim($_REQUEST['lo'])."&sort=date&limit=".$page_limit;
					}
				}
				else{
						$url=""; 
				}
				$reqdata=explode('=', $url);
				if(isset($_REQUEST['q']) && $_REQUEST['q']!=""){
						$q=$_REQUEST['q'];
				}
				elseif(isset($_REQUEST['/?q']) && $_REQUEST['/?q']!=""){
						$q=$_REQUEST['/?q'];
				}
				else{
						$q="";
				}
				 if(isset($_REQUEST['lo']) && $_REQUEST['lo']!=""){
						$l=$_REQUEST['lo'];
				}
				/*elseif(isset($_REQUEST['as_cmp']) && $_REQUEST['as_cmp']!=""){
						$q=''$_REQUEST['as_cmp'];
				}*/
				elseif(isset($_REQUEST['l']) && $_REQUEST['l']!=""){
						$l=$_REQUEST['l'];
				}
				elseif(isset($umain[1]) && $umain[1]!=""){         
						if(strrpos($umain[1], 'jobs') !== false){
							$postdata=explode('jobs', $umain[1]);
							$q=str_replace('-', ' ', $postdata[0]);
							$q=trim(str_replace('/', '', $q));
							$l=str_replace('in-', ' ', $postdata[1]);
							$l=trim(str_replace('-', ' ', $l));
						}
					 else{
							$l="";
						}
				}        
				else{
					$l="";
				}  
				if(isset($_REQUEST['viewtype']) && $_REQUEST['viewtype']!="" && $_REQUEST['viewtype']=="v")
				{
					$l="";
				}   
				//echo $l;die; 
				$pagination="";
				$filters="";
				$final=array();
				$rb_title="";
				$rr_title=array();
				$pages_counts='0';
				$noofpage='0';
				$pag_cont_text='0';
				if( $url!=""){
					$url=str_replace('+ ', '%20', $url);
					$url=str_replace(' ', '%20', $url);
					$html->load_file($url);
					$i=0;
					foreach($html->find('.result') as $element){
						foreach($element->find('.jobtitle') as $jobtitle){
							preg_match('#\\ href="(.+)\\" target#s',$jobtitle->outertext,$matches);
							$final[$i]['link']=$jobtitle->outertext;
							if(isset($matches[1])){
								$final[$i]['link']=$matches[1];
							}else{
								preg_match('#\\ href="(.+)\\" title#s',$jobtitle->outertext,$matchess);
								if(isset($matchess[1]))
								{
									$final[$i]['link']=$matchess[1];
								}else{
									$final[$i]['link']="-----"; 
								}
							}
							if(trim($jobtitle->plaintext) !="null"){ 
								$final[$i]['title']=trim($jobtitle->plaintext); 
							}else{
								$final[$i]['title']="-----"; 
							}
						}
						foreach($element->find('.company') as $jobtitle){            
							if(trim($jobtitle->plaintext) !="null"){
								$final[$i]['companyname']=trim($jobtitle->plaintext); 
							}else{
								$final[$i]['companyname']="-----"; 
							}
						}
						foreach($element->find('.location') as $jobtitle){
							if(trim($jobtitle->plaintext) !="null"){
								$final[$i]['location']=trim($jobtitle->plaintext); 
							}else{
								$final[$i]['location']="-----";  
							}
						}
						foreach($element->find('.date') as $jobtitle){
							if(trim($jobtitle->plaintext)!="null" && trim($jobtitle->plaintext)!=""){
								$final[$i]['postdate']=trim($jobtitle->plaintext); 
							}else{
								$final[$i]['postdate']="-----"; 
							}
						}          
						 if(isset($element->find('span[class="result-link-source"]', 0)->plaintext) && trim($element->find('span[class="result-link-source"]', 0)->plaintext) !="null"){              
								$final[$i]['jobsource']=trim($element->find('span[class="result-link-source"]', 0)->plaintext); 
							}else{
								$final[$i]['jobsource']="Employer Website"; 
							}
						$i++;
					}             
					foreach($html->find('#rb_Title') as $elements){               
						$rb_title= str_replace('href="','href="'.$siteurls.'?',$elements->innertext);
						$rb_title= str_replace('jobs?','?',$rb_title);
						$rb_title= str_replace('Title','Title: ',$rb_title);
						$i=0;
						foreach($elements->find('li a') as $jobtitle){
							preg_match('#\\ href="(.+)\\" title#s',$jobtitle->outertext,$matches);
							preg_match('#\\ title="(.+)\\" "#s',$jobtitle->outertext,$matches1);
							if(trim($jobtitle->innertext) !="null"){ 
								$rr_title[$i]['url']=$matches[1]; 
							}else{
								$rr_title[$i]['url']="-----"; 
							}
							$i++;
						} 
						$i=0;
						foreach($elements->find('li') as $jobtitle){
							$rr_title[$i]['title']=trim($jobtitle->plaintext); 
							$i++;
						}
					}
					foreach($html->find('.pagination') as $element){
						$pagination= str_replace('href="','href="'.$siteurls,$element->innertext);
						$pagination= str_replace('jobs?','?', $pagination);
						$pagination= str_replace('/?','?/?', $pagination);
					}
					foreach($html->find('#refineresults') as $element){
						$filters= str_replace('href="','href="'.$siteurls.'?',$element->innertext);
						$filters= str_replace('jobs?','?',$filters);
						$filters= ($filters);
					}
					// echo "<pre>"; print_r($html->find('#searchCount')); die;
					foreach($html->find('#searchCount') as $element){
						if($element->plaintext!=""){
							$pages_counts= $element->plaintext;
							$pages_counts=explode('of', $pages_counts);
							$pages_counts1=str_replace(',', '',$pages_counts[1]);
							$noofpage=ceil($pages_counts1/$page_limit);
							$pag_cont_text=$element->plaintext;
							$pag_cont_text = $pages_counts[1]." Jobs";
						}
					}          
				}
				 $l=str_replace('%20', ' ', $l);
				 $q=str_replace('%20', ' ', $q);
				 /*echo $l."<br>";
				 echo $q; exit;*/
				return view('frontend.job_list',compact('filters','pagination','final','q','l','rb_title','rr_title','pages_counts1','noofpage','pag_cont_text'));
		}
		public function getfilterJob(){
			$page_limit=env('PAGE_LIMIT','10');
			$siteurls=Request::url();
			$html = new simple_html_dom();
			$final=array();
			$umain=explode('?', base64_decode($_REQUEST['reqda']));
			if(isset($_REQUEST['filter_type']) && $_REQUEST['filter_type']=="adv"){
				$st_u=str_replace("filter_type=adv", '', $umain[1]);
				$st_u=str_replace("lo=", 'l=', $st_u);
				$url="https://www.indeed.co.uk/jobs?".$st_u."&sort=date&limit=".$page_limit;
			}
			else if(count($umain)>0 && isset($umain[1]) && $umain[1]!=""){
				if(count($umain)==3 ){
					$url="https://www.indeed.co.uk/jobs?".$umain[2]."&sort=date&limit=".$page_limit."&start=".$_REQUEST['group_no']*$page_limit;  
				}
				else{
					if(isset($_REQUEST) && $_REQUEST!="" && ((isset($_REQUEST['q']) && $_REQUEST['q']!="") || (isset($_REQUEST['lo']) && $_REQUEST['lo']!=""  ))){
						$url="https://www.indeed.co.uk/jobs?".$umain[1]."&sort=date&limit=".$page_limit."&start=".$_REQUEST['group_no']*$page_limit; 
					}
					else{
						if(strpos($umain[1],'q=')!==false){
							$url="https://www.indeed.co.uk/jobs?".$umain[1]."&sort=date&limit=".$page_limit."&start=".$_REQUEST['group_no']*$page_limit; 
						}else{
							$postdata=explode('-jobs', $umain[1]);
							$q=str_replace('-', '+', $postdata[0]);
							$q=trim(str_replace('/', '', $q));
							$l=str_replace('in-', ' ', $postdata[1]);
							$l=trim(str_replace('-', '+', $l));
							$url="https://www.indeed.co.uk/jobs?q=".$q."&l=".$l."&sort=date&limit=".$page_limit."&start=".$_REQUEST['group_no']*$page_limit; 
						}
					}
					$url=str_replace("&lo", '&l', $url);
				}
			}
			else if(isset($_REQUEST) && $_REQUEST!="" && ((isset($_REQUEST['q']) && $_REQUEST['q']!="") || (isset($_REQUEST['lo']) && $_REQUEST['lo']!=""  ))){
				$url="https://www.indeed.co.uk/jobs?q=".$_REQUEST['q']."&l=".$_REQUEST['lo']."&limit=".$page_limit."&sort=date&start=".$_REQUEST['group_no']*$page_limit;
			}
			else{
				$url=""; 
			}      
			$reqdata=explode('=', $url);      
			if(isset($_REQUEST['q']) && $_REQUEST['q']!=""){
				$q=$_REQUEST['q'];
			}
			elseif(isset($_REQUEST['/?q']) && $_REQUEST['/?q']!=""){
				$q=$_REQUEST['/?q'];
			}
			else{
				$q="";
			}
			if(isset($_REQUEST['lo']) && $_REQUEST['lo']!=""){
				$l=$_REQUEST['lo'];
			}
			elseif(isset($_REQUEST['l']) && $_REQUEST['l']!=""){
				$l=$_REQUEST['l'];
			}
			elseif(isset($umain[1]) && $umain[1]!=""){         
				if(strrpos($umain[1], 'jobs') !== false){
					$postdata=explode('jobs', $umain[1]);
					$q=str_replace('-', ' ', $postdata[0]);
					$q=trim(str_replace('/', '', $q));
					$l=str_replace('in-', ' ', $postdata[1]);
					$l=trim(str_replace('-', ' ', $l));
				}
				else{
					$l="";
				}
			}        
			else{
				$l="";
			}       
			$pagination="";
			$filters="";
			$final=array();
			$rb_title="";
			$rr_title=array();
			$pages_counts='0';
			$noofpage='0';
			$text_jobs='0';
			if( $url!=""){
				//echo $url; die;
				$url=str_replace('+ ', '%20', $url);
				$url=str_replace(' ', '%20', $url);
				$html->load_file($url);
				$i=0;
				foreach($html->find('.result') as $element){
					foreach($element->find('.jobtitle') as $jobtitle){
						preg_match('#\\ href="(.+)\\" target#s',$jobtitle->outertext,$matches);
						$final[$i]['link']=$jobtitle->outertext;
						if(isset($matches[1])){
							$final[$i]['link']=$matches[1];
						}else{
							preg_match('#\\ href="(.+)\\" title#s',$jobtitle->outertext,$matchess);
							if(isset($matchess[1])){
								$final[$i]['link']=$matchess[1];
							}else{
								$final[$i]['link']="-----"; 
							}
						}
						if(trim($jobtitle->plaintext) !="null"){ 
							$final[$i]['title']=trim($jobtitle->plaintext); 
						}else{
							$final[$i]['title']="-----"; 
						}
					}
					foreach($element->find('.company') as $jobtitle){                
						if(trim($jobtitle->plaintext) !="null"){
							$final[$i]['companyname']=trim($jobtitle->plaintext); 
						}else{
							$final[$i]['companyname']="-----"; 
						}
					}
					foreach($element->find('.location') as $jobtitle){
						if(trim($jobtitle->plaintext) !="null"){
							$final[$i]['location']=trim($jobtitle->plaintext); 
						}else{
							$final[$i]['location']="-----";  
						}
					}
					foreach($element->find('.date') as $jobtitle){
						if(trim($jobtitle->plaintext)!="null" && trim($jobtitle->plaintext)!=""){
							$final[$i]['postdate']=trim($jobtitle->plaintext); 
						}else{
							$final[$i]['postdate']="-----";  
						}
					}
					 if(isset($element->find('span[class="result-link-source"]', 0)->plaintext) && trim($element->find('span[class="result-link-source"]', 0)->plaintext) !="null"){   
								$final[$i]['jobsource']=trim($element->find('span[class="result-link-source"]', 0)->plaintext); 
							}else{
								$final[$i]['jobsource']="Employer Website"; 
							}
					$i++;
				}             
				foreach($html->find('#rb_Title') as $elements){               
					$rb_title= str_replace('href="','href="'.$siteurls.'?',$elements->innertext);
					$rb_title= str_replace('jobs?','?',$rb_title);
					$rb_title= str_replace('Title','Title: ',$rb_title);
					$i=0;
					foreach($elements->find('li a') as $jobtitle){
						preg_match('#\\ href="(.+)\\" title#s',$jobtitle->outertext,$matches);
						preg_match('#\\ title="(.+)\\" "#s',$jobtitle->outertext,$matches1);
						if(trim($jobtitle->innertext) !="null"){ 
							$rr_title[$i]['url']=$matches[1]; 
						}else{
							$rr_title[$i]['url']="-----"; 
						}
						$i++;
					} 
					$i=0;
					foreach($elements->find('li') as $jobtitle){
						$rr_title[$i]['title']=trim($jobtitle->plaintext); 
						$i++;
					}
				}
				foreach($html->find('.pagination') as $element){
					$pagination= str_replace('href="','href="'.$siteurls,$element->innertext);
					$pagination= str_replace('jobs?','?', $pagination);
					$pagination= str_replace('/?','?/?', $pagination);
				}
				foreach($html->find('#refineresults') as $element){
					$filters= str_replace('href="','href="'.$siteurls.'?',$element->innertext);
					$filters= str_replace('jobs?','?',$filters);
					$filters= ($filters);
				}
				foreach($html->find('#searchCount') as $element){
					if($element->plaintext!=""){
						$pages_counts= $element->plaintext;
						$pages_counts=explode('of', $pages_counts);
						$pages_counts1=str_replace(',', '',$pages_counts[1]);
						$noofpage=ceil($pages_counts1/$page_limit);
						$pages= explode('to',$pages_counts[0]);
						$text_jobs=$pages_counts1." Jobs";
					}
				}          
			}
			$l=str_replace('%20', ' ', $l);
				 $q=str_replace('%20', ' ', $q);
			return view('frontend.job_list_filter',compact('filters','pagination','final','q','l','rb_title','rr_title','pages_counts1','noofpage'))."[{[". $text_jobs;

		}
		public function getresumes()
		{
				$page_limit=env('PAGE_LIMIT','10');
				 $login_url = env('INDEED_LOGIN_URL');
				 $email = env('EMAIL');
				 $pass = env('PASSWORD');
				$post_data = "email=".$email."&password=".$pass."&login_tk=1btlneotibdfjb1e&pvr=0&surftok=6v1ceg38N3UZ2SFadWj4FbUpcDxDets7&tmpl=&cfb=0&action=login";
				$ch = curl_init();
				$agent = $_SERVER["HTTP_USER_AGENT"];
				curl_setopt($ch, CURLOPT_USERAGENT, $agent);
				curl_setopt($ch, CURLOPT_URL, $login_url );
				curl_setopt($ch, CURLOPT_POST, 1 );
				curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
				curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

				/*
				Set the cookie storing files
				Cookie files are necessary since we are logging and session data needs to be saved
				*/

				//curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.txt');
				curl_setopt($ch, CURLOPT_COOKIEFILE, '../cookie.txt');
				$postResult = curl_exec($ch);

			 // $secure_url = 'https://www.indeed.com/resumes?q=Java+Developer&l=Bristol';
			 // $secure_url = 'https://www.indeed.com/resumes?q=php&l=';

			 
			 // echo '<pre>';print_r($result);echo '</pre>';
			 // echo 'done';//
				$siteurls=Request::url()."/";
				$html = new simple_html_dom();
				$final=array();
				$umain=explode('?', $_SERVER['REQUEST_URI']);
				if(count($umain)>1 ){
					if(count($umain)==3 ){
						$url="https://www.indeed.com/resumes?".$umain[2]."&sort=date&cb=jt";
					}
					else{
						//echo "2";
						if(isset($_REQUEST) && $_REQUEST!="" && ((isset($_REQUEST['q']) && $_REQUEST['q']!="") || (isset($_REQUEST['lo']) && $_REQUEST['lo']!=""  ))){
							if(isset($_REQUEST['titleLast']))
							{
								$umain[1]="q=".trim($_REQUEST['q'])."+ title:".trim($_REQUEST['titleLast'])."&filter_type=adv&lo=".trim($_REQUEST['lo']);
							}
						 // echo $umain[1];
							$url="https://www.indeed.com/resumes?".$umain[1]."&sort=date&cb=jt"; 
						}
						else{
							$url="https://www.indeed.com/".$umain[1]; 
						}
						$url=str_replace("&lo", '&l', $url);
					}
				}
				else if(isset($_REQUEST) && $_REQUEST!="" && ((isset($_REQUEST['q']) && $_REQUEST['q']!="") || (isset($_REQUEST['lo']) && $_REQUEST['lo']!=""  )) ){
					$url="https://www.indeed.com/resumes?cb=jt&q=".$_REQUEST['q']."&l=".$_REQUEST['lo']."&sort=date";
				}
				else{
					 $url=""; 
				}
				$q="";
				if( isset($_REQUEST) && $_REQUEST!="" && isset($_REQUEST['filter_type']) && $_REQUEST['filter_type']=="adv")
				{
					$strs="";
				 $companyquery="";
				 $titlequery="";
					if($_REQUEST['jobtitle_type']=="Any")
					{
							$titlequery='anytitle:'.$_REQUEST['titleLast'];
					}else{
						 $titlequery='title:'.$_REQUEST['titleLast'];
					}
					if($_REQUEST['Company_type']=="Any")
					{
							$companyquery='anycompany:'.$_REQUEST['companyLast'];
					}
					else{
						 $companyquery='company:'.$_REQUEST['companyLast'];
					}
					if($_REQUEST['companyLast']=="")
					{
						 $companyquery="";
					}
					if($_REQUEST['titleLast']=="")
					{
						 $titlequery="";
					}
					 $url="https://www.indeed.com/resumes?q=".$_REQUEST['q']." ".$titlequery." ".$companyquery. "&l=".$_REQUEST['lo']."&sort=date&cb=jt";
					 $q=$_REQUEST['q']." ".$titlequery." ".$companyquery;
				}
			 //echo $url; die;
				$reqdata=explode('=', $url);
				if($q!="")
				{
					$q=$q;
				}
				elseif(isset($_REQUEST['q']) && $_REQUEST['q']!=""){
					$q=$_REQUEST['q'];
				}
				elseif(isset($_REQUEST['/?q']) && $_REQUEST['/?q']!=""){
					 $q=$_REQUEST['/?q'];
				}
				else{
					$q="";
				}
				if(isset($_REQUEST['lo']) && $_REQUEST['lo']!=""){
					$l=$_REQUEST['lo'];
				}
				elseif(isset($_REQUEST['l']) && $_REQUEST['l']!=""){
					 $l=$_REQUEST['l'];
				}
				else{
					$l="";
				}
				//echo $q; 
				$pagination="";
				$filters="";
				$rb_title="";
				$pag_cont_text="";
				$noofpage="";
				$final=array();
				$rr_title=array();
				if($url!=""){
					 $secure_url=$url;
					$html->load_file($url);
				// echo $url;die;
					//curl_setopt($ch, CURLOPT_URL, $secure_url);
					//$result = curl_exec($ch);
				 // $html=str_get_html($result);
				//  echo '<pre>';print_r($result);echo '</pre>';die;
					$i=0;
					foreach($html->find('.sre') as $element){
					 foreach($element->find('.app_name') as $jobtitle){
						preg_match('#\\ href="(.+)\\" rel#s',$jobtitle->outertext,$matches);
						$final[$i]['link']=$jobtitle->outertext;
						if(isset($matches[1])){
							$final[$i]['link']=$matches[1];
						}else{
							preg_match('#\\ href="(.+)\\" title#s',$jobtitle->outertext,$matchess);
							if(isset($matchess[1])){
								$final[$i]['link']=$matchess[1];
							}else{
								$final[$i]['link']="-----"; 
							}
						}
						 if(isset($jobtitle->find('a[class="app_link"]', 0)->plaintext) && trim($jobtitle->find('a[class="app_link"]', 0)->plaintext) !="null"){ 
							$final[$i]['title']=trim(str_replace('- Recently Updated','',$jobtitle->find('a[class="app_link"]', 0)->plaintext)); 
						}else{
							$final[$i]['title']="-----"; 
						}
					}
					foreach($element->find('.experience') as $jobtitle){                
						if(trim($jobtitle->plaintext) !="null"){
							$final[$i]['companyname']=trim($jobtitle->plaintext); 
						}else{
							$final[$i]['companyname']="-----"; 
						}
						break;
					}
					if(isset($element->find('span[class="location"]', 0)->plaintext) && trim($element->find('span[class="location"]', 0)->plaintext) !="null"){ 
							$final[$i]['location']=trim(str_replace('-','',$element->find('span[class="location"]', 0)->plaintext)); 
						}else{
							$final[$i]['location']="-----"; 
						}
					foreach($element->find('.last_updated') as $jobtitle){
						if(trim($jobtitle->plaintext)!="null" && trim($jobtitle->plaintext)!=""){
							$final[$i]['postdate']=trim(str_replace('Updated:','',$jobtitle->plaintext)); 
						}else{
							$final[$i]['postdate']="-----";  
						}
					} 
					if(isset($element->find('div[class="education"]', 0)->plaintext) && trim($element->find('div[class="education"]', 0)->plaintext) !="null"){ 
							$final[$i]['education']=trim($element->find('div[class="education"]', 0)->plaintext); 
						}else{
							$final[$i]['education']="-----"; 
						}
					 if(isset($element->find('span[class="result-link-source"]', 0)->plaintext) && trim($element->find('span[class="result-link-source"]', 0)->plaintext) !="null"){   
								$final[$i]['jobsource']=trim($jobtitle->plaintext); 
							}else{
								$final[$i]['jobsource']="-----"; 
							}
					$i++;
					}  $i=0;
					foreach($html->find('ul[data-group-key="jtid"]') as $elements){               
				 $rb_title= str_replace('href="','href="'.$siteurls.'?',$elements->innertext);
					$rb_title= str_replace('jobs?','?',$rb_title);
					$rb_title= str_replace('Title','Title: ',$rb_title);
					$i=0;
					foreach($elements->find('li a') as $jobtitle){
						preg_match('#\\ href="(.+)\\" data-tn-element#s',$jobtitle->outertext,$matches);
					 // preg_match('#\\ data-tn-element="(.+)\\" "#s',$jobtitle->outertext,$matches1);
						if(trim($jobtitle->innertext) !="null"){ 
							$rr_title[$i]['url']=$matches[1]; 
						}else{
							$rr_title[$i]['url']="-----"; 
						}
						$i++;
					}
				 $i=0;
					foreach($elements->find('li') as $jobtitle){
						$rr_title[$i]['title']=trim($jobtitle->plaintext); 
						$i++;
					}
				}
					//echo "<pre>";print_r($rr_title);die;
					foreach($html->find('#pagination') as $element){
						$pagination= str_replace('href="','href="'.$siteurls,$element->innertext);
						$pagination= str_replace('jobs?','?', $pagination);
						$pagination= str_replace('/?','?', $pagination);
					}
					foreach($html->find('#refinements') as $element){
						$filters= str_replace('href="','href="'.$siteurls.'',$element->innertext);
						$filters= str_replace('jobs?','?',$filters);
						$filters= str_replace('/?','?',$filters);
						$filters= str_replace('<p class="refine_title">Distance</p>','<p class="refine_title2">Distance</p>',$filters);
						$filters= str_replace('<p class="refine_title">Last Updated</p>','<p class="refine_title2">Last Updated</p>',$filters);
					}
				
				foreach($html->find('#result_count') as $element){
					//echo $element->plaintext;die;
					if($element->plaintext!=""){
						$pages_counts= $element->plaintext;
						$pages_counts=str_replace('resumes', '', $pages_counts);
						 $pages_counts1=str_replace(' CVs', '',$pages_counts);
						 $pages_counts1=str_replace(',', '',$pages_counts1);
						$noofpage=ceil($pages_counts1/$page_limit); 
						//$pages= explode('to',$pages_counts[0]);
						//$text_jobs="Jobs 1 to ".$pages[1]." of ".$pages_counts1;
						 $pag_cont_text=$element->plaintext;
					}
				} 
				}
				//print_r($final);die;
				return view('frontend.resumes_list',compact('filters','pagination','final','q','l','rb_title','pag_cont_text','noofpage','rr_title'));
		}
		public function getfilterresume()
		{
		$page_limit=env('PAGE_LIMIT','10');
				 $login_url = env('INDEED_LOGIN_URL');
				 $email = env('EMAIL');
				 $pass = env('PASSWORD');
				$post_data = "email=".$email."&password=".$pass."&login_tk=1btlneotibdfjb1e&pvr=0&surftok=6v1ceg38N3UZ2SFadWj4FbUpcDxDets7&tmpl=&cfb=0&action=login";
				$ch = curl_init();
				$agent = $_SERVER["HTTP_USER_AGENT"];
				curl_setopt($ch, CURLOPT_USERAGENT, $agent);
				curl_setopt($ch, CURLOPT_URL, $login_url );
				curl_setopt($ch, CURLOPT_POST, 1 );
				curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
				curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

				/*
				Set the cookie storing files
				Cookie files are necessary since we are logging and session data needs to be saved
				*/

				//curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.txt');
				curl_setopt($ch, CURLOPT_COOKIEFILE, '../cookie.txt');
				$postResult = curl_exec($ch);

			 // $secure_url = 'https://www.indeed.com/resumes?q=Java+Developer&l=Bristol';
			 // $secure_url = 'https://www.indeed.com/resumes?q=php&l=';

			 
			 // echo '<pre>';print_r($result);echo '</pre>';
			 // echo 'done';//
			$siteurls=Request::url();
			$html = new simple_html_dom();
			$final=array();
			$umain=explode('?', base64_decode($_REQUEST['reqda']));
			if(isset($_REQUEST['filter_type']) && $_REQUEST['filter_type']=="adv"){
				$st_u=str_replace("filter_type=adv", '', $umain[1]);
				$st_u=str_replace("lo=", 'l=', $st_u);
				$url="https://www.indeed.com/resumes?".$st_u."&sort=date&cb=jt&start=".$_REQUEST['group_no']*$page_limit;
			}
			else if(count($umain)>0 && isset($umain[1]) && $umain[1]!=""){
				if(count($umain)==3 ){
					$url="https://www.indeed.com/resumes?".$umain[2]."&sort=date&cb=jt&start=".$_REQUEST['group_no']*$page_limit;  
				}
				else{
					if(isset($_REQUEST) && $_REQUEST!="" && ((isset($_REQUEST['q']) && $_REQUEST['q']!="") || (isset($_REQUEST['lo']) && $_REQUEST['lo']!=""  ))){
						$url="https://www.indeed.com/resumes?".$umain[1]."&sort=date&cb=jt&start=".$_REQUEST['group_no']*$page_limit; 
					}
					else{
						if(strpos($umain[1],'q=')!==false){
							$url="https://www.indeed.com/resumes?".$umain[1]."&sort=date&cb=jt&start=".$_REQUEST['group_no']*$page_limit; 
						}else{
							$postdata=explode('-jobs', $umain[1]);
							$q=str_replace('-', '+', $postdata[0]);
							$q=trim(str_replace('/', '', $q));
							$l=str_replace('in-', ' ', $postdata[1]);
							$l=trim(str_replace('-', '+', $l));
							$url="https://www.indeed.com/resumes?q=".$q."&l=".$l."&sort=date&cb=jt&start=".$_REQUEST['group_no']*$page_limit; 
						}
					}
					$url=str_replace("&lo", '&l', $url);
				}
			}
			else if(isset($_REQUEST) && $_REQUEST!="" && ((isset($_REQUEST['q']) && $_REQUEST['q']!="") || (isset($_REQUEST['lo']) && $_REQUEST['lo']!=""  ))){
				$url="https://www.indeed.com/resumes?cb=jt&sort=date&q=".$_REQUEST['q']."&l=".$_REQUEST['lo'];
			}    
			else{
				$url=""; 
			} 
			if( isset($_REQUEST) && $_REQUEST!="" && isset($_REQUEST['filter_type']) && $_REQUEST['filter_type']=="adv")
				{
					$strs="";
				 $companyquery="";
				 $titlequery="";
					if($_REQUEST['jobtitle_type']=="Any")
					{
							$titlequery='anytitle:'.$_REQUEST['titleLast'];
					}else{
						 $titlequery='title:'.$_REQUEST['titleLast'];
					}
					if($_REQUEST['Company_type']=="Any")
					{
							$companyquery='anycompany:'.$_REQUEST['companyLast'];
					}
					else{
						 $companyquery='company:'.$_REQUEST['companyLast'];
					}
					if($_REQUEST['companyLast']=="")
					{
						 $companyquery="";
					}
					if($_REQUEST['titleLast']=="")
					{
						 $titlequery="";
					}
					 $url="https://www.indeed.com/resumes?q=".$_REQUEST['q']." ".$titlequery." ".$companyquery. "&l=".$_REQUEST['lo']."&sort=date&cb=jt";
					 $q=$_REQUEST['q']." ".$titlequery." ".$companyquery;
				}
						
			$reqdata=explode('=', $url);      
			if(isset($_REQUEST['q']) && $_REQUEST['q']!=""){
				$q=$_REQUEST['q'];
			}
			elseif(isset($_REQUEST['/?q']) && $_REQUEST['/?q']!=""){
				$q=$_REQUEST['/?q'];
			}
			else{
				$q="";
			}
			if(isset($_REQUEST['lo']) && $_REQUEST['lo']!=""){
				$l=$_REQUEST['lo'];
			}
			elseif(isset($_REQUEST['l']) && $_REQUEST['l']!=""){
				$l=$_REQUEST['l'];
			}
			elseif(isset($umain[1]) && $umain[1]!=""){         
				if(strrpos($umain[1], 'jobs') !== false){
					$postdata=explode('jobs', $umain[1]);
					$q=str_replace('-', ' ', $postdata[0]);
					$q=trim(str_replace('/', '', $q));
					$l=str_replace('in-', ' ', $postdata[1]);
					$l=trim(str_replace('-', ' ', $l));
				}
				else{
					$l="";
				}
			}        
			else{
				$l="";
			}       
			$pagination="";
			$filters="";
			$final=array();
			$rb_title="";
			$rr_title=array();
			$pages_counts='0';
			$noofpage='0';
			$text_jobs='0';
			if( $url!=""){

				//echo $url; die;
				$url=str_replace('+ ', '%20', $url);
				$url=str_replace(' ', '%20', $url);
				$html->load_file($url);
			// $secure_url=$url; 
			// curl_setopt($ch, CURLOPT_URL, $secure_url);
				 // $result = curl_exec($ch);
				 // $html=str_get_html($result);
				$i=0;
				foreach($html->find('.sre') as $element){
					 foreach($element->find('.app_name') as $jobtitle){
						preg_match('#\\ href="(.+)\\" rel#s',$jobtitle->outertext,$matches);
						$final[$i]['link']=$jobtitle->outertext;
						if(isset($matches[1])){
							$final[$i]['link']=$matches[1];
						}else{
							preg_match('#\\ href="(.+)\\" title#s',$jobtitle->outertext,$matchess);
							if(isset($matchess[1])){
								$final[$i]['link']=$matchess[1];
							}else{
								$final[$i]['link']="-----"; 
							}
						}
						 if(isset($jobtitle->find('a[class="app_link"]', 0)->plaintext) && trim($jobtitle->find('a[class="app_link"]', 0)->plaintext) !="null"){ 
							$final[$i]['title']=trim(str_replace('- Recently Updated','',$jobtitle->find('a[class="app_link"]', 0)->plaintext)); 
						}else{
							$final[$i]['title']="-----"; 
						}
					}
					foreach($element->find('.experience') as $jobtitle){                
						if(trim($jobtitle->plaintext) !="null"){
							$final[$i]['companyname']=trim($jobtitle->plaintext); 
						}else{
							$final[$i]['companyname']="-----"; 
						}
						break;
					}
					if(isset($element->find('span[class="location"]', 0)->plaintext) && trim($element->find('span[class="location"]', 0)->plaintext) !="null"){ 
							$final[$i]['location']=trim(str_replace('-','',$element->find('span[class="location"]', 0)->plaintext)); 
						}else{
							$final[$i]['location']="-----";  
						}
					foreach($element->find('.last_updated') as $jobtitle){
						if(trim($jobtitle->plaintext)!="null" && trim($jobtitle->plaintext)!=""){
							$final[$i]['postdate']=trim(str_replace('Updated:','',$jobtitle->plaintext)); 
						}else{
							$final[$i]['postdate']="-----";  
						}
					} 
					if(isset($element->find('div[class="education"]', 0)->plaintext) && trim($element->find('div[class="education"]', 0)->plaintext) !="null"){ 
							$final[$i]['education']=trim($element->find('div[class="education"]', 0)->plaintext); 
						}else{
							$final[$i]['education']="-----";  
						}
					 if(isset($element->find('span[class="result-link-source"]', 0)->plaintext) && trim($element->find('span[class="result-link-source"]', 0)->plaintext) !="null"){   
								$final[$i]['jobsource']=trim($jobtitle->plaintext); 
							}else{
								$final[$i]['jobsource']="-----"; 
							}
					$i++;
					}    
				foreach($html->find('#rb_Title') as $elements){               
					$rb_title= str_replace('href="','href="'.$siteurls.'?',$elements->innertext);
					$rb_title= str_replace('jobs?','?',$rb_title);
					$rb_title= str_replace('Title','Title: ',$rb_title);
					$i=0;
					foreach($elements->find('li a') as $jobtitle){
						preg_match('#\\ href="(.+)\\" title#s',$jobtitle->outertext,$matches);
						preg_match('#\\ title="(.+)\\" "#s',$jobtitle->outertext,$matches1);
						if(trim($jobtitle->innertext) !="null"){ 
							$rr_title[$i]['url']=$matches[1]; 
						}else{
							$rr_title[$i]['url']="-----"; 
						}
						$i++;
					} 
					$i=0;
					foreach($elements->find('li') as $jobtitle){
						$rr_title[$i]['title']=trim($jobtitle->plaintext); 
						$i++;
					}
				}
				foreach($html->find('.pagination') as $element){
					$pagination= str_replace('href="','href="'.$siteurls,$element->innertext);
					$pagination= str_replace('jobs?','?', $pagination);
					$pagination= str_replace('/?','?/?', $pagination);
				}
				foreach($html->find('#refineresults') as $element){
					$filters= str_replace('href="','href="'.$siteurls.'?',$element->innertext);
					$filters= str_replace('jobs?','?',$filters);
					$filters= ($filters);
				}
				foreach($html->find('#searchCount') as $element){
					if($element->plaintext!=""){
						$pages_counts= $element->plaintext;
						$pages_counts=explode('of', $pages_counts);
						$pages_counts1=str_replace(',', '',$pages_counts[1]);
						$noofpage=ceil($pages_counts1/$page_limit);
						$pages= explode('to',$pages_counts[0]);
						$text_jobs="Jobs 1 to ".$pages[1]." of ".$pages_counts1;
					}
				}          
			}
			$l=str_replace('%20', ' ', $l);
				 $q=str_replace('%20', ' ', $q);
					return view('frontend.resumes_list_filter',compact('filters','pagination','final','q','l','rb_title','pag_cont_text','noofpage'));
		}
		public function getjobtitle($value='')
		{
			$output ="";
			$st=$_REQUEST['term'];
			$str_json=file_get_contents("https://www.indeed.co.uk/rpc/suggest?what=true&from=hp&tk=1bupt4ufu148n1jg&cb=what_ac.cb&q=".$st."&caret=".strlen($st));  
			$output = preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {
			return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
			}, $str_json);

			$output = str_replace('/**/what_ac.cb','',$output);
			$center = substr($output, strpos($output, ', [') + 3);
			$output = substr($center, 0, strpos($center, ']],'));       
			$output="[".$output."]";
			$output=json_decode($output);
			$final_arr=array();
			if(count( $output)>0){
				foreach($output as $k=>$key){
					$key1 = str_replace("<b>","" ,$key);
						$key1 = str_replace("</b>","" ,$key1);
					 $final_arr[$k]['id']=ucfirst($key);
					 $final_arr[$k]['label']=ucfirst($key);
					 $final_arr[$k]['value']=ucfirst($key1);
				}
			}
			return $_REQUEST['callback'].'('.json_encode($final_arr).")";exit;
		}
		public function getjoblocation($value='')
		{
			$output ="";
			$st=$_REQUEST['term'];
			$str_json=file_get_contents("https://www.indeed.co.uk/rpc/suggest?from=hp&tk=1bupt4ufu148n1jg&cb=where_ac.cb&q=".$st."&caret=".strlen($st));  
			$output = preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {
			return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
			}, $str_json);
			$output = str_replace('/**/where_ac.cb','',$output);
			$center = substr($output, strpos($output, ', [') + 3);
			$output = substr($center, 0, strpos($center, ']]'));
			$output="[".$output."]";
			$output=json_decode($output);      
			$final_arr=array();
			if(count( $output)>0){
				foreach($output as $k=>$key){
					$key1 = str_replace("<b>","" ,$key);
						$key1 = str_replace("</b>","" ,$key1);
					 $final_arr[$k]['id']=ucfirst($key);
					 $final_arr[$k]['label']=ucfirst($key);
					 $final_arr[$k]['value']=ucfirst($key1);
				}
			}
			 return $_REQUEST['callback'].'('.json_encode($final_arr).")";exit;      
		}
		public function getjoblocation1($value='')
		{
			$output ="";
			$st=$_REQUEST['term'];
			$str_json=file_get_contents("https://www.indeed.co.uk/rpc/suggest?from=hp&tk=1bupt4ufu148n1jg&cb=where_ac.cb&q=".$st."&caret=".strlen($st));  
			$output = preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {
			return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
			}, $str_json);
			$output = str_replace('/**/where_ac.cb','',$output);
			$center = substr($output, strpos($output, ', [') + 3);
			$output = substr($center, 0, strpos($center, ']]'));
			$output="[".$output."]";
			$output=json_decode($output);      
			$final_arr=array();
			if(count( $output)>0){
				foreach($output as $k=>$key){
					$key1 = str_replace("<b>","" ,$key);
						$key1 = str_replace("</b>","" ,$key1);
					 $final_arr[$k]['id']=ucfirst($key);
					 $final_arr[$k]['label']=ucfirst($key);
					 $final_arr[$k]['value']=ucfirst($key1);
				}
			}
			 return $_REQUEST['callback'].'('.json_encode($final_arr).")";exit;
		}
		public function advance_search()
		{
				return view('frontend.adv_job');
		}
		public function fav_job()
		{
			if(isset($_REQUEST['term']) && $_REQUEST['term']!=""){
				if($_REQUEST['type']=="fav"){
					$inserdata=array('link'=> base64_decode($_REQUEST['term']),'user_id'=>Auth::user()->id,'type'=>'job');
					DB::table('whish_lists')->insert($inserdata);
				}else{
					$job_arr=DB::table('whish_lists')->where('link',base64_decode($_REQUEST['term']))->first();
					if(!empty($job_arr)){
						$con_arr=array('link'=>base64_decode($_REQUEST['term']),'user_id'=>Auth::user()->id);
						DB::table('whish_lists')->where($con_arr)->delete();
					}
				}
				return "true";
			}
		}
		 public function fav_resume()
		{
			if(isset($_REQUEST['term']) && $_REQUEST['term']!=""){
				if($_REQUEST['type']=="fav"){
					$inserdata=array('link'=> base64_decode($_REQUEST['term']),'user_id'=>Auth::user()->id,'type'=>'resume');
					DB::table('whish_lists')->insert($inserdata);
				}else{
					$job_arr=DB::table('whish_lists')->where('link',base64_decode($_REQUEST['term']))->first();
					if(!empty($job_arr)){
						$con_arr=array('link'=>base64_decode($_REQUEST['term']),'user_id'=>Auth::user()->id);
						DB::table('whish_lists')->where($con_arr)->delete();
					}
				}
				return "true";
			}
		}
		public function getDashboard()
		{
			$html = new simple_html_dom();
			$parseurl = 'https://www.indeed.co.uk/jobtrends/category-trends';
			$html->load_file($parseurl);
			$items = array();
			$i=0;
		 // $items[$i]['category_name'] = 'Category name';
		 // $items[$i]['job_postings_number'] = 'Job postings number';
		 // $items[$i]['category_url'] = 'Category url';
			//$items[$i]['top_data_filename'] = 'Top data file name';
		 // $i++;
			$new_final = array();
			foreach($html->find('div[class="job-category-link-container"]') as $article) {
			//var_dump($article);exit;
			$items[$i]['category_name'] = trim($article->find('div[class="category-name"]', 0)->plaintext);
			
			if(isset($article->find('span[class="category-change positive"]', 0)->plaintext) && trim($article->find('span[class="category-change positive"]', 0)->plaintext) !="null"){ 
				$items[$i]['cat_ch_positive']=trim($article->find('span[class="category-change positive"]', 0)->plaintext); 
			}else{
				$items[$i]['cat_ch_positive']="";
			}
		 
			if(isset($article->find('span[class="category-change negative"]', 0)->plaintext) && trim($article->find('span[class="category-change negative"]', 0)->plaintext) !="null"){ 
				$items[$i]['cat_ch_negative']=trim($article->find('span[class="category-change negative"]', 0)->plaintext); 
			}else{
				$items[$i]['cat_ch_negative']="";
			}
			// $items[$i]['cat_ch_po'] = trim($article->find('span[class="category-change positive"]', 0)->plaintext);
			// $items[$i]['cat_ch_ne'] = trim($article->find('span[class="category-change negative"]', 0)->plaintext);
			$a=trim($article->find('div[class="job-postings-number"]', 0)->innertext,' ');    
			$items[$i]['job_postings_number'] = trim(substr($a,0,10));    
			$items[$i]['category_url'] = str_replace('/jobtrends','',trim($article->find('a[class="job-category-link"]', 0)->href));
			$items[$i]['top_data_filename'] = $items[$i]['category_name'].'.csv';
			#update Category
			$last_year='--';
			if(!empty($items[$i]['cat_ch_positive']))
			{
				$last_year = $items[$i]['cat_ch_positive'];
			}
			if(!empty($items[$i]['cat_ch_negative']))
			{
				$last_year = $items[$i]['cat_ch_negative'];
			}
			
			if(date('d') == 1)
			{

				$con = array('cat_name'=> $items[$i]['category_name'], 'jobposting'=> $items[$i]['job_postings_number'], 'last_year'=> $last_year);
				DB::table('category')->where('cat_url',$items[$i]['category_url'])->Update($con);
			}
			#update Category
			//echo "<pre>"; print_r($items[$i]);
			$i++;      
			}
			//exit;
			if(isset($items) && count($items)>0)
			{
				$items=array_chunk($items, 4);
			}
			// clean up memory
			$html->clear();
			unset($html);
			//echo "<pre>";print_r($items);die;
			return view('frontend.dashboard',compact('items'));
		}
		public function dashboard_datails($caturl,$ctaname)
		{    
			 $html = new simple_html_dom();
			$parseurl = 'https://www.indeed.co.uk/jobtrends/'.$caturl;
			$html->load_file($parseurl);
			$items = array();
		
	$i=0; 
	$new_final = array();
		foreach($html->find('div[id="job-postings"]') as $article) {
				$a=trim($article->find('div[id="job-postings-number"]', 0)->innertext,' ');    
				$items[$i]['job_postings_number'] = trim(substr($a,0,10)); 
				foreach($article->find('div[id="job-postings-mom-change"]') as $article1){
				
				if(isset($article1->find('span[class="category-change positive"]', 0)->plaintext) && trim($article1->find('span[class="category-change positive"]', 0)->plaintext) !="null"){ 
					$items[$i]['cat_ch_positive']=trim($article1->find('span[class="category-change positive"]', 0)->plaintext); 
				}else{
					$items[$i]['cat_ch_positive']="";
				}

				if(isset($article1->find('span[class="category-change negative"]', 0)->plaintext) && trim($article1->find('span[class="category-change negative"]', 0)->plaintext) !="null"){ 
					$items[$i]['cat_ch_negative']=trim($article1->find('span[class="category-change negative"]', 0)->plaintext); 
				}else{
					$items[$i]['cat_ch_negative']="";
				}  
			}
			 foreach($article->find('div[id="job-postings-yoy-change"]') as $article1){        
				if(isset($article1->find('span[class="category-change positive"]', 0)->plaintext) && trim($article1->find('span[class="category-change positive"]', 0)->plaintext) !="null"){ 
					$items[$i]['cat_ch_positivey']=trim($article1->find('span[class="category-change positive"]', 0)->plaintext); 
				}else{
					$items[$i]['cat_ch_positivey']="";
				}

				if(isset($article1->find('span[class="category-change negative"]', 0)->plaintext) && trim($article1->find('span[class="category-change negative"]', 0)->plaintext) !="null"){ 
					$items[$i]['cat_ch_negativey']=trim($article1->find('span[class="category-change negative"]', 0)->plaintext); 
				}else{
					$items[$i]['cat_ch_negativey']="";
				}  
			}
				 $perclickdata = $this->scraping_category_job_digg($i,$ctaname, $parseurl, '');
		}
		$html->clear();
		unset($html);
			return view('frontend.dashboard1',compact('perclickdata','items','ctaname'));
		}

	public function scraping_category_job_digg($p,$category_name=NULL, $category_job_url=NULL, $category_job_filename=NULL) {
		$html = file_get_html($category_job_url);
		$items = array();
		$i=0;
		foreach($html->find('div[class="Top-List"]') as $article) {
			$j=0;
			$mm = '';
			$mm = $category_name;
			foreach($article->find('tr[class="data-row"]') as $chld) {
				$items[0][$j] = $category_name; 
				$s=$i+1;
				if($i==0){
					$items[1][$j] = trim($chld->find('td[class="name"]', 0)->plaintext);  
					$items[2][$j] = trim($chld->find('td[class="value"]', 0)->plaintext); 
				}elseif($i==1){
					$items[3][$j] = trim($chld->find('td[class="name"]', 0)->plaintext);  
					$items[4][$j] = trim($chld->find('td[class="value"]', 0)->plaintext);
				}else{
					$items[5][$j] = trim($chld->find('td[class="name"]', 0)->plaintext);  
					$items[6][$j] = trim($chld->find('td[class="value"]', 0)->plaintext);
				}
				$j++;
			}
			$i++;   
		}
		$html->clear();
		unset($html);
		return $items;
	}
	public function guided_analysis()
	{
		 $cat_data=DB::table('category')->get();
		 //echo "<pre>";print_r($cat_data);die;
		return view('frontend.guidedanalysis',compact('cat_data'));
	}
	public function guided_analysis_update()
	{
		$items=$this->all_category(); 
		
		$cat_data=array();
		foreach($items as $k=>$v)
		{      
			$cat_data[$k]=$v;
			$trend_datas=$this->trends_data($v['category_url'],$v['category_name']);
			 $cat_data[$k]['trends_cat_ch_positive']=$trend_datas[0]['cat_ch_positive'];
			 $cat_data[$k]['trends_cat_ch_negative']=$trend_datas[0]['cat_ch_negative'];
			 $cat_data[$k]['trends_cat_ch_positivey']=$trend_datas[0]['cat_ch_positivey'];
			 $cat_data[$k]['trends_cat_ch_negativey']=$trend_datas[0]['cat_ch_negativey'];

			if($trend_datas[0]['cat_ch_positive']!="")
				$last_month=$trend_datas[0]['cat_ch_positive'];
			elseif($trend_datas[0]['cat_ch_negative']!="")
				 $last_month= $trend_datas[0]['cat_ch_negative'];
			else
				 $last_month= '----';
			

			if($trend_datas[0]['cat_ch_positivey']!="")
				$last_year=$trend_datas[0]['cat_ch_positivey'];
			elseif($trend_datas[0]['cat_ch_negativey']!="")
				 $last_year= $trend_datas[0]['cat_ch_negativey'];
			else
				 $last_year= '----';
			

			$insert_arr=array('cat_name'=>$v['category_name'],'cat_url'=>$v['category_url'],'jobposting'=>$v['job_postings_number'],'last_year'=>$last_year,'last_month'=>$last_month);


			DB::table('category')->insert($insert_arr);

			 //break;
		}   
	}
	public function trends_data($cat_url='',$ctaname='')
	{
		$html = new simple_html_dom();
		$parseurl = 'https://www.indeed.co.uk/jobtrends/'.$cat_url;
		$html->load_file($parseurl);
		$items = array();
		$i=0; 
		$new_final = array();
		foreach($html->find('div[id="job-postings"]') as $article) {
			$a=trim($article->find('div[id="job-postings-number"]', 0)->innertext,' ');    
			$items[$i]['job_postings_number'] = trim(substr($a,0,10)); 
			foreach($article->find('div[id="job-postings-mom-change"]') as $article1){
				if(isset($article1->find('span[class="category-change positive"]', 0)->plaintext) && trim($article1->find('span[class="category-change positive"]', 0)->plaintext) !="null"){ 
					$items[$i]['cat_ch_positive']=trim($article1->find('span[class="category-change positive"]', 0)->plaintext); 
				}else{
					$items[$i]['cat_ch_positive']="";
				}

				if(isset($article1->find('span[class="category-change negative"]', 0)->plaintext) && trim($article1->find('span[class="category-change negative"]', 0)->plaintext) !="null"){ 
					$items[$i]['cat_ch_negative']=trim($article1->find('span[class="category-change negative"]', 0)->plaintext); 
				}else{
					$items[$i]['cat_ch_negative']="";
				}  
			}
			foreach($article->find('div[id="job-postings-yoy-change"]') as $article1){        
				if(isset($article1->find('span[class="category-change positive"]', 0)->plaintext) && trim($article1->find('span[class="category-change positive"]', 0)->plaintext) !="null"){ 
					$items[$i]['cat_ch_positivey']=trim($article1->find('span[class="category-change positive"]', 0)->plaintext); 
				}else{
					$items[$i]['cat_ch_positivey']="";
				}

				if(isset($article1->find('span[class="category-change negative"]', 0)->plaintext) && trim($article1->find('span[class="category-change negative"]', 0)->plaintext) !="null"){ 
					$items[$i]['cat_ch_negativey']=trim($article1->find('span[class="category-change negative"]', 0)->plaintext); 
				}else{
					$items[$i]['cat_ch_negativey']="";
				}  
			}
			$perclickdata = $this->scraping_category_job_digg($i,$ctaname, $parseurl, '');
		}
		$html->clear();
		return $items;
	}
	public function trends_datails($caturl,$ctaname)
	{    
		$html = new simple_html_dom();
		$parseurl = 'https://www.indeed.co.uk/jobtrends/'.$caturl;
		$html->load_file($parseurl);
		$items = array();
		$i=0; 
		$new_final = array();
		foreach($html->find('div[id="job-postings"]') as $article) {
			$a=trim($article->find('div[id="job-postings-number"]', 0)->innertext,' ');    
			$items[$i]['job_postings_number'] = trim(substr($a,0,10)); 
			foreach($article->find('div[id="job-postings-mom-change"]') as $article1){
				if(isset($article1->find('span[class="category-change positive"]', 0)->plaintext) && trim($article1->find('span[class="category-change positive"]', 0)->plaintext) !="null"){ 
					$items[$i]['cat_ch_positive']=trim($article1->find('span[class="category-change positive"]', 0)->plaintext); 
				}else{
					$items[$i]['cat_ch_positive']="";
				}

				if(isset($article1->find('span[class="category-change negative"]', 0)->plaintext) && trim($article1->find('span[class="category-change negative"]', 0)->plaintext) !="null"){ 
					$items[$i]['cat_ch_negative']=trim($article1->find('span[class="category-change negative"]', 0)->plaintext); 
				}else{
					$items[$i]['cat_ch_negative']="";
				}  
			}
			foreach($article->find('div[id="job-postings-yoy-change"]') as $article1){        
				if(isset($article1->find('span[class="category-change positive"]', 0)->plaintext) && trim($article1->find('span[class="category-change positive"]', 0)->plaintext) !="null"){ 
					$items[$i]['cat_ch_positivey']=trim($article1->find('span[class="category-change positive"]', 0)->plaintext); 
				}else{
					$items[$i]['cat_ch_positivey']="";
				}

				if(isset($article1->find('span[class="category-change negative"]', 0)->plaintext) && trim($article1->find('span[class="category-change negative"]', 0)->plaintext) !="null"){ 
					$items[$i]['cat_ch_negativey']=trim($article1->find('span[class="category-change negative"]', 0)->plaintext); 
				}else{
					$items[$i]['cat_ch_negativey']="";
				}  
			}
			$perclickdata = $this->scraping_category_job_digg($i,$ctaname, $parseurl, '');
		}
		$html->clear();
		unset($html);
		$all_category=$this->all_category();
		return view('frontend.trends_detaits',compact('perclickdata','items','ctaname','all_category'));
	}
	public function salary_watch()
	{
		 return view('frontend.salary_watch');
	}
	public function all_category($value='')
	{
		$html = new simple_html_dom();
		$parseurl = 'https://www.indeed.co.uk/jobtrends/category-trends';
		$html->load_file($parseurl);
		$items = array();
		$i=0;
		$new_final = array();
		foreach($html->find('div[class="job-category-link-container"]') as $article) {
			$items[$i]['category_name'] = trim($article->find('div[class="category-name"]', 0)->plaintext);      
			if(isset($article->find('span[class="category-change positive"]', 0)->plaintext) && trim($article->find('span[class="category-change positive"]', 0)->plaintext) !="null"){ 
				$items[$i]['cat_ch_positive']=trim($article->find('span[class="category-change positive"]', 0)->plaintext); 
			}else{
				$items[$i]['cat_ch_positive']="";
			}
		 
			if(isset($article->find('span[class="category-change negative"]', 0)->plaintext) && trim($article->find('span[class="category-change negative"]', 0)->plaintext) !="null"){ 
				$items[$i]['cat_ch_negative']=trim($article->find('span[class="category-change negative"]', 0)->plaintext); 
			}else{
				$items[$i]['cat_ch_negative']="";
			}
			$a=trim($article->find('div[class="job-postings-number"]', 0)->innertext,' ');    
			$items[$i]['job_postings_number'] = trim(substr($a,0,10));    
			$items[$i]['category_url'] = str_replace('/jobtrends','',trim($article->find('a[class="job-category-link"]', 0)->href));
			$items[$i]['top_data_filename'] = $items[$i]['category_name'].'.csv';
			$i++;      
		}
		$html->clear();
		unset($html);
		return  $items;
	}


/* Salary Section start */
public function storesal_data($value='')
{

}
public function getsal_jobtitle($value='')
{
			$output ="";
			$st=trim($_REQUEST['term']);
			//$str_json=file_get_contents("https://www.indeed.co.uk/rpc/suggest?from=hp&tk=1bupt4ufu148n1jg&cb=where_ac.cb&q=".$st."&caret=".strlen($st));  
			$str_json=file_get_contents("https://www.indeed.co.uk/cmp/_/salary-search?q=".$st."&caret=".strlen($st));  
			//  $output = str_replace('/**/where_ac.cb','',$output);
			 /*$output=json_decode($str_json);  
			 print_r($output);die;
			$output = preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {
			return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
			}, $str_json);
		
			$center = substr($output, strpos($output, ', [') + 3);
			$output = substr($center, 0, strpos($center, ']]'));
			$output="[".$output."]";*/
			$output=json_decode($str_json);      
			$final_arr=array();
			if(count( $output)>0){
				foreach($output as $k=>$key){
					$key1 = str_replace("<b>","" ,$key->text);
						$key1 = str_replace("</b>","" ,$key1);
					 $final_arr[$k]['id']=ucfirst($key->text);
					 $final_arr[$k]['label']=ucfirst($key->text);
					 $final_arr[$k]['link']=ucfirst($key->link);
					 $final_arr[$k]['value']=ucfirst($key1);
				}
			}
			 return $_REQUEST['callback'].'('.json_encode($final_arr).")";exit;
}
/* Salary Section end */

# T Function Start----------------------------------------
	public function analysis_guided()
	{
		 $cat_data=DB::table('category')->get();
		 //echo "<pre>";print_r($cat_data);die;
		return view('frontend.pages.guidedanalysis',compact('cat_data'));
	}

	public function getjob()
		{
			$x='';
				$siteurls=Request::url();
				$html = new simple_html_dom();
				$final=array();
				$umain=explode('?', $_SERVER['REQUEST_URI']);
				$page_limit=env('PAGE_LIMIT','10');
				if(isset($_REQUEST['filter_type']) && $_REQUEST['filter_type']=="adv"){
					$st_u=str_replace("filter_type=adv", '', $umain[1]);
					$st_u=str_replace("lo=", 'l=', $st_u);
					$url="https://www.indeed.co.uk/jobs?".$st_u."&sort=date&limit=".$page_limit;
				}
				else if(count($umain)>0 && isset($umain[1]) && $umain[1]!=""){
						if(count($umain)==3 ){
								$url="https://www.indeed.co.uk/jobs?".$umain[2]."&sort=date&limit=".$page_limit;  
						}
						else{
							if(isset($_REQUEST) && $_REQUEST!="" && ((isset($_REQUEST['q']) && $_REQUEST['q']!="") || (isset($_REQUEST['lo']) && $_REQUEST['lo']!=""  ) || (isset($_REQUEST['as_cmp']) && $_REQUEST['as_cmp']!=""  ))){
									$url="https://www.indeed.co.uk/jobs?".$umain[1]."&sort=date&limit=".$page_limit; 
									if(isset($_REQUEST['as_cmp']) && $_REQUEST['as_cmp']!=""  )
									{
										$url="https://www.indeed.co.uk/jobs?as_cmp=".$_REQUEST['as_cmp']."&l=".trim($_REQUEST['l'])."&sort=date&limit=".$page_limit;
									}
							}
							else{
									$url="https://www.indeed.co.uk/".$umain[1]; 
									//echo "else".$url; exit;                      
							}
							$url=str_replace("&lo", '&l', $url);
						}
				}
				else if(isset($_REQUEST) && $_REQUEST!="" && ((isset($_REQUEST['q']) && $_REQUEST['q']!="") || (isset($_REQUEST['lo']) && $_REQUEST['lo']!=""  ) || (isset($_REQUEST['as_cmp']) && $_REQUEST['as_cmp']!=""  ))){

					$url="https://www.indeed.co.uk/jobs?q=".$_REQUEST['q']."&l=".trim($_REQUEST['lo'])."&sort=date&limit=".$page_limit;
					if(isset($_REQUEST['as_cmp']) && $_REQUEST['as_cmp']!=""  )
					{
						$url="https://www.indeed.co.uk/jobs?as_cmp=".$_REQUEST['as_cmp']."&l=".trim($_REQUEST['lo'])."&sort=date&limit=".$page_limit;
					}
				}
				else{
						$url=""; 
				}
				$reqdata=explode('=', $url);
				//echo "<pre>"; print_r($reqdata); exit;
				if(isset($_REQUEST['q']) && $_REQUEST['q']!=""){
						$q=$_REQUEST['q'];
				}
				elseif(isset($_REQUEST['/?q']) && $_REQUEST['/?q']!=""){
						$q=$_REQUEST['/?q'];
				}
				else{
					//echo "else";
						$q="";
				}
				 if(isset($_REQUEST['lo']) && $_REQUEST['lo']!=""){
						$l=$_REQUEST['lo'];
				}
				/*elseif(isset($_REQUEST['as_cmp']) && $_REQUEST['as_cmp']!=""){
						$q=''$_REQUEST['as_cmp'];
				}*/
				elseif(isset($_REQUEST['l']) && $_REQUEST['l']!=""){
						$l=$_REQUEST['l'];
				}
				elseif(isset($umain[1]) && $umain[1]!=""){         
					//echo "else-if";
						if(strrpos($umain[1], 'jobs') !== false){
							//echo "if"; exit;
							$postdata=explode('jobs', $umain[1]);
							$q=str_replace('-', ' ', $postdata[0]);
							$q=trim(str_replace('/', '', $q));
							$l=str_replace('in-', ' ', $postdata[1]);
							$l=trim(str_replace('-', ' ', $l));
							$x=1;
//              echo $q.'-'.$l; exit;
						}
					 else{
							$l="";
						}
				}        
				else{
					$l="";
				}  
				if(isset($_REQUEST['viewtype']) && $_REQUEST['viewtype']!="" && $_REQUEST['viewtype']=="v")
				{
					$l="";
				}   
				//echo $l;die; 
				$pagination="";
				$filters="";
				$final=array();
				$rb_title="";
				$rr_title=array();
				$pages_counts='0';
				$noofpage='0';
				$pag_cont_text='0';
				//echo $url; exit;
				if( $url!=""){
					$url=str_replace('+ ', '%20', $url);
					$url=str_replace(' ', '%20', $url);
					$html->load_file($url);
					$i=0;
					foreach($html->find('.result') as $element){
						foreach($element->find('.jobtitle') as $jobtitle){
							preg_match('#\\ href="(.+)\\" target#s',$jobtitle->outertext,$matches);
							$final[$i]['link']=$jobtitle->outertext;
							if(isset($matches[1])){
								$final[$i]['link']=$matches[1];
							}else{
								preg_match('#\\ href="(.+)\\" title#s',$jobtitle->outertext,$matchess);
								if(isset($matchess[1]))
								{
									$final[$i]['link']=$matchess[1];
								}else{
									$final[$i]['link']="-----"; 
								}
							}
							if(trim($jobtitle->plaintext) !="null"){ 
								$final[$i]['title']=trim($jobtitle->plaintext); 
							}else{
								$final[$i]['title']="-----"; 
							}
						}
						foreach($element->find('.company') as $jobtitle){            
							if(trim($jobtitle->plaintext) !="null"){
								$final[$i]['companyname']=trim($jobtitle->plaintext); 
							}else{
								$final[$i]['companyname']="-----"; 
							}
						}
						foreach($element->find('.location') as $jobtitle){
							if(trim($jobtitle->plaintext) !="null"){
								$final[$i]['location']=trim($jobtitle->plaintext); 
							}else{
								$final[$i]['location']="-----";  
							}
						}
						foreach($element->find('.date') as $jobtitle){
							if(trim($jobtitle->plaintext)!="null" && trim($jobtitle->plaintext)!=""){
								$final[$i]['postdate']=trim($jobtitle->plaintext); 
							}else{
								$final[$i]['postdate']="-----"; 
							}
						}          
						 if(isset($element->find('span[class="result-link-source"]', 0)->plaintext) && trim($element->find('span[class="result-link-source"]', 0)->plaintext) !="null"){              
								$final[$i]['jobsource']=trim($element->find('span[class="result-link-source"]', 0)->plaintext); 
							}else{
								$final[$i]['jobsource']="Employer Website"; 
							}
						$i++;
					}             
					foreach($html->find('#rb_Title') as $elements){               
						$rb_title= str_replace('href="','href="'.$siteurls.'?',$elements->innertext);
						$rb_title= str_replace('jobs?','?',$rb_title);
						$rb_title= str_replace('Title','Title: ',$rb_title);
						$i=0;
						foreach($elements->find('li a') as $jobtitle){
							preg_match('#\\ href="(.+)\\" title#s',$jobtitle->outertext,$matches);
							preg_match('#\\ title="(.+)\\" "#s',$jobtitle->outertext,$matches1);
							if(trim($jobtitle->innertext) !="null"){ 
								$rr_title[$i]['url']=$matches[1]; 
							}else{
								$rr_title[$i]['url']="-----"; 
							}
							$i++;
						} 
						$i=0;
						foreach($elements->find('li') as $jobtitle){
							$rr_title[$i]['title']=trim($jobtitle->plaintext); 
							$i++;
						}
					}
					foreach($html->find('.pagination') as $element){
						$pagination= str_replace('href="','href="'.$siteurls,$element->innertext);
						$pagination= str_replace('jobs?','?', $pagination);
						$pagination= str_replace('/?','?/?', $pagination);
					}
					foreach($html->find('#refineresults') as $element){
						$filters= str_replace('href="','href="'.$siteurls.'?',$element->innertext);
						$filters= str_replace('jobs?','?',$filters);
						$filters= ($filters);
					}
					// echo "<pre>"; print_r($html->find('#searchCount')); die;
					foreach($html->find('#searchCount') as $element){
						if($element->plaintext!=""){
							$pages_counts= $element->plaintext;
							$pages_counts=explode('of', $pages_counts);
							$pages_counts1=str_replace(',', '',$pages_counts[1]);
							$noofpage=ceil($pages_counts1/$page_limit);
							$pag_cont_text=$element->plaintext;
							$pag_cont_text = $pages_counts[1]." Jobs";
						}
					}          
				}
				 $l=str_replace('%20', ' ', $l);
				 $q=str_replace('%20', ' ', $q);
				 //echo "<pre>"; print_r($final); exit;
				return view('frontend.pages.job_list',compact('filters','pagination','final','q','l','rb_title','rr_title','pages_counts1','noofpage','pag_cont_text','x'));
		}

		public function getjobmyjobscount($tval=NULL)
		{   
				//echo $l;die; 
				$pagination="";
				$filters="";
				$final=array();
				$rb_title="";
				$rr_title=array();
				$pages_counts='0';
				$noofpage='0';
				$pag_cont_text='0';
				 $siteurls=Request::url();
				$html = new simple_html_dom();
				$final=array();
				//http://localhost/indeed/www/public/myjob?as_cmp=IT-Talent-Solutions&l=Fleet&viewtype=v
				$url=$tval;
				//echo $url;
				if( $url!=""){
					$url=str_replace('+ ', '%20', $url);
					$url=str_replace(' ', '%20', $url);
					$html->load_file($url);
					$i=0;
					foreach($html->find('.result') as $element){
						foreach($element->find('.jobtitle') as $jobtitle){
							preg_match('#\\ href="(.+)\\" target#s',$jobtitle->outertext,$matches);
							$final[$i]['link']=$jobtitle->outertext;
							if(isset($matches[1])){
								$final[$i]['link']=$matches[1];
							}else{
								preg_match('#\\ href="(.+)\\" title#s',$jobtitle->outertext,$matchess);
								if(isset($matchess[1]))
								{
									$final[$i]['link']=$matchess[1];
								}else{
									$final[$i]['link']="-----"; 
								}
							}
							if(trim($jobtitle->plaintext) !="null"){ 
								$final[$i]['title']=trim($jobtitle->plaintext); 
							}else{
								$final[$i]['title']="-----"; 
							}
						}
						foreach($element->find('.company') as $jobtitle){            
							if(trim($jobtitle->plaintext) !="null"){
								$final[$i]['companyname']=trim($jobtitle->plaintext); 
							}else{
								$final[$i]['companyname']="-----"; 
							}
						}
						foreach($element->find('.location') as $jobtitle){
							if(trim($jobtitle->plaintext) !="null"){
								$final[$i]['location']=trim($jobtitle->plaintext); 
							}else{
								$final[$i]['location']="-----";  
							}
						}
						foreach($element->find('.date') as $jobtitle){
							if(trim($jobtitle->plaintext)!="null" && trim($jobtitle->plaintext)!=""){
								$final[$i]['postdate']=trim($jobtitle->plaintext); 
							}else{
								$final[$i]['postdate']="-----"; 
							}
						}          
						 if(isset($element->find('span[class="result-link-source"]', 0)->plaintext) && trim($element->find('span[class="result-link-source"]', 0)->plaintext) !="null"){              
								$final[$i]['jobsource']=trim($element->find('span[class="result-link-source"]', 0)->plaintext); 
							}else{
								$final[$i]['jobsource']="Employer Website"; 
							}
						$i++;
					}             
					foreach($html->find('#rb_Title') as $elements){               
						$rb_title= str_replace('href="','href="'.$siteurls.'?',$elements->innertext);
						$rb_title= str_replace('jobs?','?',$rb_title);
						$rb_title= str_replace('Title','Title: ',$rb_title);
						$i=0;
						foreach($elements->find('li a') as $jobtitle){
							preg_match('#\\ href="(.+)\\" title#s',$jobtitle->outertext,$matches);
							preg_match('#\\ title="(.+)\\" "#s',$jobtitle->outertext,$matches1);
							if(trim($jobtitle->innertext) !="null"){ 
								$rr_title[$i]['url']=$matches[1]; 
							}else{
								$rr_title[$i]['url']="-----"; 
							}
							$i++;
						} 
						$i=0;
						foreach($elements->find('li') as $jobtitle){
							$rr_title[$i]['title']=trim($jobtitle->plaintext); 
							$i++;
						}
					}
					foreach($html->find('.pagination') as $element){
						$pagination= str_replace('href="','href="'.$siteurls,$element->innertext);
						$pagination= str_replace('jobs?','?', $pagination);
						$pagination= str_replace('/?','?/?', $pagination);
					}
					foreach($html->find('#refineresults') as $element){
						$filters= str_replace('href="','href="'.$siteurls.'?',$element->innertext);
						$filters= str_replace('jobs?','?',$filters);
						$filters= ($filters);
					}
					// echo "<pre>"; print_r($html->find('#searchCount')); die;
					/*foreach($html->find('#searchCount') as $element){
						if($element->plaintext!=""){
							$pages_counts= $element->plaintext;
							$pages_counts=explode('of', $pages_counts);
							$pages_counts1=str_replace(',', '',$pages_counts[1]);
							$noofpage=ceil($pages_counts1/$page_limit);
							$pag_cont_text=$element->plaintext;
							$pag_cont_text = $pages_counts[1]." Jobs";
						}
					} */         
				}
				 //$l=str_replace('%20', ' ', $l);
				 //$q=str_replace('%20', ' ', $q);
				/*if(!empty($final))
				{
					count($final);
				}
				else
				{
					$final=0;
				}*/

				 return $final;
				//return view('frontend.pages.job_list',compact('filters','pagination','final','q','l','rb_title','rr_title','pages_counts1','noofpage','pag_cont_text','x'));
		}

# T Function End-------------------------------------------

		#--------------------- Import Retail Start -----------------------------
	public function getImportRetail(){
		include_once(public_path().'/simple_html_dom.php');
		$final=array();
		$umain=explode('?', $_SERVER['REQUEST_URI']);
		$page_limit=env('PAGE_LIMIT','10');
		$pagination="";
		$filters="";
		$final=array();
		$rb_title="";
		$rr_title=array();
		$pages_counts='0';
		$noofpage='0';
		$pag_cont_text='0';
		$z=0;
		$url = "https://www.indeed.co.uk/jobs?q=retail";
		$html = file_get_html($url);
		$countdata='';
		foreach($html->find('div[id="searchCount"]') as $element)
		{
			$count_data = $element->plaintext;
			list($pp,$countdata) = explode('of',$count_data);
			$countdata = str_replace(',','',trim($countdata));
		}
		$list=0;
		if(!empty($countdata))
		{
			$list='';
			$list = $countdata/10;
		}
		$start = 0;
		$csv_header='';
		$csv_row='';
		$i=0;	
		for ($k=$start; $k <=$list ; $k++) 
		{ 
			if($k==0)
			{
				$url = "https://www.indeed.co.uk/jobs?q=retail";
			}
			else
			{
				$url = "https://www.indeed.co.uk/jobs?q=retail&start=".$k*10;
			}
			
			$html->load_file($url);
			foreach($html->find('.result') as $element)
			{
				foreach($element->find('.jobtitle') as $jobtitle)
				{
					preg_match('#\\ href="(.+)\\" target#s',$jobtitle->outertext,$matches);
					$final[$i]['link']=$jobtitle->outertext;
					if(isset($matches[1]))
					{
						$final[$i]['link']=$matches[1];
					}
					else
					{
						preg_match('#\\ href="(.+)\\" title#s',$jobtitle->outertext,$matchess);
						if(isset($matchess[1]))
						{
							$final[$i]['link']=$matchess[1];
						}
						else
						{
							$final[$i]['link']="-----";
						}
					}
					if(trim($jobtitle->plaintext) !="null")
					{
						$final[$i]['title']=trim($jobtitle->plaintext);
					}
					else
					{
						$final[$i]['title']="-----";
					}
				}
				foreach($element->find('.company') as $jobtitle)
				{
					if(trim($jobtitle->plaintext) !="null")
					{
						$final[$i]['companyname']=trim($jobtitle->plaintext);
					}
					else
					{
						$final[$i]['companyname']="-----";
					}
				}
				foreach($element->find('.location') as $jobtitle)
				{
					if(trim($jobtitle->plaintext) !="null")
					{
						$final[$i]['location']=trim($jobtitle->plaintext);
					}
					else
					{
						$final[$i]['location']="-----";
					}
				}
				foreach($element->find('.date') as $jobtitle)
				{
					if(trim($jobtitle->plaintext)!="null" && trim($jobtitle->plaintext)!="")
					{
						$final[$i]['postdate']=trim($jobtitle->plaintext);
					}
					else
					{
						$final[$i]['postdate']="-----";
					}
				}
				if(isset($element->find('span[class="result-link-source"]', 0)->plaintext) && trim($element->find('span[class="result-link-source"]', 0)->plaintext) !="null")
				{
					$final[$i]['jobsource']=trim($element->find('span[class="result-link-source"]', 0)->plaintext);
				}
				else
				{
					$final[$i]['jobsource']="Employer Website"; 
				}
				$i++;
			}			
			/* foreach ($final as $key1 => $value) {
				foreach ($value as $k1 => $v1) {
					if($k == 0  && $key1 == 0){
						$csv_header .= '"' . $k1 . '",';
										
					}
					$csv_row .= '"' . $v1. '",';
				}
				if($k == 0 && $key1 == 0){
					$csv_header .= "\n";	
				}
				$csv_row .= "\n";
			} */
		}
		
		$fp = fopen('/var/www/html/discover.infini8.co.uk/public/export_csv_file/jobs.csv', 'w');
		foreach ( $final as $line ) {
			//$val = explode(",", $line);
			fputcsv($fp, $line);
		}
		fclose($fp);
		
		/* header('Content-type:application/csv');
        header('Content-Disposition:attachment;filename='.url("/").'/export_csv_file/watches_file.csv');
		echo $csv_header . $csv_row; */
		exit;
	}

		
	public function getImportRetail_old()
	{
		$siteurls=Request::url();
		//echo $siteurls."<br>"; 
		$html = new simple_html_dom();
		$final=array();
		$umain=explode('?', $_SERVER['REQUEST_URI']);
		$page_limit=env('PAGE_LIMIT','10');
		if(isset($_REQUEST['filter_type']) && $_REQUEST['filter_type']=="adv")
		{
			$st_u=str_replace("filter_type=adv", '', $umain[1]);
			$st_u=str_replace("lo=", 'l=', $st_u);
			$url="https://www.indeed.co.uk/jobs?".$st_u."&sort=date&limit=".$page_limit;
		}
		else if(count($umain)>0 && isset($umain[1]) && $umain[1]!="")
		{
			if(count($umain)==3 )
			{
				$url="https://www.indeed.co.uk/jobs?".$umain[2]."&sort=date&limit=".$page_limit;
			}
			else
			{
				if(isset($_REQUEST) && $_REQUEST!="" && ((isset($_REQUEST['q']) && $_REQUEST['q']!="") || (isset($_REQUEST['lo']) && $_REQUEST['lo']!=""  ) || (isset($_REQUEST['as_cmp']) && $_REQUEST['as_cmp']!=""  )))
				{
					$url="https://www.indeed.co.uk/jobs?".$umain[1]."&sort=date&limit=".$page_limit;
					if(isset($_REQUEST['as_cmp']) && $_REQUEST['as_cmp']!=""  )
					{
						$url="https://www.indeed.co.uk/jobs?as_cmp=".$_REQUEST['as_cmp']."&l=".trim($_REQUEST['l'])."&sort=date&limit=".$page_limit;
					}
				}
				else
				{
					$url="https://www.indeed.co.uk/".$umain[1];
				}
				$url=str_replace("&lo", '&l', $url);
			}
		}
		else if(isset($_REQUEST) && $_REQUEST!="" && ((isset($_REQUEST['q']) && $_REQUEST['q']!="") || (isset($_REQUEST['lo']) && $_REQUEST['lo']!=""  ) || (isset($_REQUEST['as_cmp']) && $_REQUEST['as_cmp']!=""  )))
		{
			$url="https://www.indeed.co.uk/jobs?q=".$_REQUEST['q']."&l=".trim($_REQUEST['lo'])."&sort=date&limit=".$page_limit;
			if(isset($_REQUEST['as_cmp']) && $_REQUEST['as_cmp']!=""  )
			{
				$url="https://www.indeed.co.uk/jobs?as_cmp=".$_REQUEST['as_cmp']."&l=".trim($_REQUEST['lo'])."&sort=date&limit=".$page_limit;
			}
		}
		else
		{
			$url="";
		}
		$reqdata=explode('=', $url);
		if(isset($_REQUEST['q']) && $_REQUEST['q']!="")
		{
			$q=$_REQUEST['q'];
		}
		elseif(isset($_REQUEST['/?q']) && $_REQUEST['/?q']!="")
		{
			$q=$_REQUEST['/?q'];
		}
		else
		{
			$q="";
		}
		if(isset($_REQUEST['lo']) && $_REQUEST['lo']!="")
		{
			$l=$_REQUEST['lo'];
		}
		elseif(isset($_REQUEST['l']) && $_REQUEST['l']!="")
		{
			$l=$_REQUEST['l'];
		}
		elseif(isset($umain[1]) && $umain[1]!="")
		{
			if(strrpos($umain[1], 'jobs') !== false)
			{
				$postdata=explode('jobs', $umain[1]);
				$q=str_replace('-', ' ', $postdata[0]);
				$q=trim(str_replace('/', '', $q));
				$l=str_replace('in-', ' ', $postdata[1]);
				$l=trim(str_replace('-', ' ', $l));
			}
			else
			{
				$l="";
			}
		}
		else
		{
			$l="";
		}
		if(isset($_REQUEST['viewtype']) && $_REQUEST['viewtype']!="" && $_REQUEST['viewtype']=="v")
		{
			$l="";
		}
		$pagination="";
		$filters="";
		$final=array();
		$rb_title="";
		$rr_title=array();
		$pages_counts='0';
		$noofpage='0';
		$pag_cont_text='0';
		$z=0;

		if( $url!="")
		{
			//echo $url."<br>";
			$i=0;
			
			echo $url = "https://www.indeed.co.uk/jobs?q=retail";				exit;
			$url=str_replace('+ ', '%20', $url);
			$url=str_replace(' ', '%20', $url);
			$html->load_file($url);
			foreach($html->find('.result') as $element)
			{
				foreach($element->find('.jobtitle') as $jobtitle)
				{
					preg_match('#\\ href="(.+)\\" target#s',$jobtitle->outertext,$matches);
					$final[$i]['link']=$jobtitle->outertext;
					if(isset($matches[1]))
					{
						$final[$i]['link']=$matches[1];
					}
					else
					{
						preg_match('#\\ href="(.+)\\" title#s',$jobtitle->outertext,$matchess);
						if(isset($matchess[1]))
						{
							$final[$i]['link']=$matchess[1];
						}
						else
						{
							$final[$i]['link']="-----";
						}
					}
					if(trim($jobtitle->plaintext) !="null")
					{
						$final[$i]['title']=trim($jobtitle->plaintext);
					}
					else
					{
						$final[$i]['title']="-----";
					}
				}
				foreach($element->find('.company') as $jobtitle)
				{
					if(trim($jobtitle->plaintext) !="null")
					{
						$final[$i]['companyname']=trim($jobtitle->plaintext);
					}
					else
					{
						$final[$i]['companyname']="-----";
					}
				}
				foreach($element->find('.location') as $jobtitle)
				{
					if(trim($jobtitle->plaintext) !="null")
					{
						$final[$i]['location']=trim($jobtitle->plaintext);
					}
					else
					{
						$final[$i]['location']="-----";
					}
				}
				foreach($element->find('.date') as $jobtitle)
				{
					if(trim($jobtitle->plaintext)!="null" && trim($jobtitle->plaintext)!="")
					{
						$final[$i]['postdate']=trim($jobtitle->plaintext);
					}
					else
					{
						$final[$i]['postdate']="-----";
					}
				}
				if(isset($element->find('span[class="result-link-source"]', 0)->plaintext) && trim($element->find('span[class="result-link-source"]', 0)->plaintext) !="null")
				{
					$final[$i]['jobsource']=trim($element->find('span[class="result-link-source"]', 0)->plaintext);
				}
				else
				{
					$final[$i]['jobsource']="Employer Website"; 
				}
				$i++;
			}
			foreach($html->find('#rb_Title') as $elements)
			{
				$rb_title= str_replace('href="','href="'.$siteurls.'?',$elements->innertext);
				$rb_title= str_replace('jobs?','?',$rb_title);
				$rb_title= str_replace('Title','Title: ',$rb_title);
				$i=0;
				foreach($elements->find('li a') as $jobtitle)
				{
					preg_match('#\\ href="(.+)\\" title#s',$jobtitle->outertext,$matches);
					preg_match('#\\ title="(.+)\\" "#s',$jobtitle->outertext,$matches1);
					if(trim($jobtitle->innertext) !="null")
					{ 
						$rr_title[$i]['url']=$matches[1]; 
					}
					else
					{
						$rr_title[$i]['url']="-----"; 
					}
					$i++;
				} 
				$i=0;
				foreach($elements->find('li') as $jobtitle)
				{
					$rr_title[$i]['title']=trim($jobtitle->plaintext); 
					$i++;
				}
			}
			foreach($html->find('.pagination') as $element)
			{
				$pagination= str_replace('href="','href="'.$siteurls,$element->innertext);
				$pagination= str_replace('jobs?','?', $pagination);
				$pagination= str_replace('/?','?/?', $pagination);
			}
			foreach($html->find('#refineresults') as $element)
			{
				$filters= str_replace('href="','href="'.$siteurls.'?',$element->innertext);
				$filters= str_replace('jobs?','?',$filters);
				$filters= ($filters);
			}
			foreach($html->find('#searchCount') as $element)
			{
				if($element->plaintext!="")
				{
					$pages_counts= $element->plaintext;
					$pages_counts=explode('of', $pages_counts);
					$pages_counts1=str_replace(',', '',$pages_counts[1]);
					$noofpage=ceil($pages_counts1/$page_limit);
					$pag_cont_text=$element->plaintext;
					$pag_cont_text = $pages_counts[1]." Jobs";
				}
			}

			$totalz =  $pages_counts1/10;


			for ($z=1; $i < 5; $z++) {
				$url = "https://www.indeed.co.uk/jobs?q=retail&start=".$z*10;
				//echo $url;	
				$i=0;			
				$url=str_replace('+ ', '%20', $url);
				$url=str_replace(' ', '%20', $url);
				$html->load_file($url);
				foreach($html->find('.result') as $element)
				{
					foreach($element->find('.jobtitle') as $jobtitle)
					{
						preg_match('#\\ href="(.+)\\" target#s',$jobtitle->outertext,$matches);
						$final[$i]['link']=$jobtitle->outertext;
						if(isset($matches[1]))
						{
							$final[$i]['link']=$matches[1];
						}
						else
						{
							preg_match('#\\ href="(.+)\\" title#s',$jobtitle->outertext,$matchess);
							if(isset($matchess[1]))
							{
								$final[$i]['link']=$matchess[1];
							}
							else
							{
								$final[$i]['link']="-----";
							}
						}
						if(trim($jobtitle->plaintext) !="null")
						{
							$final[$i]['title']=trim($jobtitle->plaintext);
						}
						else
						{
							$final[$i]['title']="-----";
						}
					}
					foreach($element->find('.company') as $jobtitle)
					{
						if(trim($jobtitle->plaintext) !="null")
						{
							$final[$i]['companyname']=trim($jobtitle->plaintext);
						}
						else
						{
							$final[$i]['companyname']="-----";
						}
					}
					foreach($element->find('.location') as $jobtitle)
					{
						if(trim($jobtitle->plaintext) !="null")
						{
							$final[$i]['location']=trim($jobtitle->plaintext);
						}
						else
						{
							$final[$i]['location']="-----";
						}
					}
					foreach($element->find('.date') as $jobtitle)
					{
						if(trim($jobtitle->plaintext)!="null" && trim($jobtitle->plaintext)!="")
						{
							$final[$i]['postdate']=trim($jobtitle->plaintext);
						}
						else
						{
							$final[$i]['postdate']="-----";
						}
					}
					if(isset($element->find('span[class="result-link-source"]', 0)->plaintext) && trim($element->find('span[class="result-link-source"]', 0)->plaintext) !="null")
					{
						$final[$i]['jobsource']=trim($element->find('span[class="result-link-source"]', 0)->plaintext);
					}
					else
					{
						$final[$i]['jobsource']="Employer Website"; 
					}
					$i++;

				}
				foreach($html->find('#rb_Title') as $elements)
				{
					$rb_title= str_replace('href="','href="'.$siteurls.'?',$elements->innertext);
					$rb_title= str_replace('jobs?','?',$rb_title);
					$rb_title= str_replace('Title','Title: ',$rb_title);
					$i=0;
					foreach($elements->find('li a') as $jobtitle)
					{
						preg_match('#\\ href="(.+)\\" title#s',$jobtitle->outertext,$matches);
						preg_match('#\\ title="(.+)\\" "#s',$jobtitle->outertext,$matches1);
						if(trim($jobtitle->innertext) !="null")
						{ 
							$rr_title[$i]['url']=$matches[1]; 
						}
						else
						{
							$rr_title[$i]['url']="-----"; 
						}
						$i++;
					} 
					$i=0;
					foreach($elements->find('li') as $jobtitle)
					{
						$rr_title[$i]['title']=trim($jobtitle->plaintext); 
						$i++;
					}
				}
				foreach($html->find('.pagination') as $element)
				{
					$pagination= str_replace('href="','href="'.$siteurls,$element->innertext);
					$pagination= str_replace('jobs?','?', $pagination);
					$pagination= str_replace('/?','?/?', $pagination);
				}
				foreach($html->find('#refineresults') as $element)
				{
					$filters= str_replace('href="','href="'.$siteurls.'?',$element->innertext);
					$filters= str_replace('jobs?','?',$filters);
					$filters= ($filters);
				}
				/*foreach($html->find('#searchCount') as $element)
				{
					if($element->plaintext!="")
					{
						$pages_counts= $element->plaintext;
						$pages_counts=explode('of', $pages_counts);
						$pages_counts1=str_replace(',', '',$pages_counts[1]);
						$noofpage=ceil($pages_counts1/$page_limit);
						$pag_cont_text=$element->plaintext;
						$pag_cont_text = $pages_counts[1]." Jobs";
					}
				}*/
				echo "<pre>"; print_r($final);
			}


		}






		$l=str_replace('%20', ' ', $l);
		$q=str_replace('%20', ' ', $q);
		$csv_header='';
		$csv_row='';
		foreach ($final as $key => $value) {
			//echo "<pre>"; print_r($value);
			foreach ($value as $k => $v) {
				if($key == 0){
					$csv_header .= '"' . $k . '",';
									
				}
				$csv_row .= '"' . $v . '",';
			}
			if($key == 0){
				$csv_header .= "\n";	
			}
			$csv_row .= "\n";
		}
		 header('Content-type:application/csv');
          header('Content-Disposition:attachment;filename='.url("/").'/export_csv_file/watches_file.csv');
          echo $csv_header . $csv_row;
          
		exit;


		//return view('frontend.job_list',compact('filters','pagination','final','q','l','rb_title','rr_title','pages_counts1','noofpage','pag_cont_text'));
	}

		#--------------------- Import Retail End -----------------------------

}
