<?php

/**
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */

Route::get('macros', 'FrontendController@macros')->name('macros');
Route::get('storesal_data/{query?}', 'FrontendController@storesal_data')->name('storesal_data');
Route::get('getsal_jobtitle/{query?}', 'FrontendController@getsal_jobtitle')->name('getsal_jobtitle');
Route::get('getfilterJob/{query?}', 'FrontendController@getfilterJob')->name('getfilterJob');
Route::get('getfilterresume/{query?}', 'FrontendController@getfilterresume')->name('getfilterresume');

/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 */
    #  Import Retail Start
    Route::get('retail/{query?}', 'FrontendController@getImportRetail')->name('retail');
    # Import Retail End
Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'FrontendController@index')->name('index');
    Route::get('contact', 'ContactController@index')->name('contact');
    Route::get('myjob/{query?}', 'FrontendController@getjobs')->name('myjob');
    Route::get('getresumes/{query?}', 'FrontendController@getresumes')->name('getresumes');
    Route::get('getjobtitle/{query?}', 'FrontendController@getjobtitle')->name('getjobtitle');
    Route::get('advance_search', 'FrontendController@advance_search')->name('advance_search');
    Route::get('getjoblocation/{query?}', 'FrontendController@getjoblocation')->name('getjoblocation');
    Route::get('getjoblocation1/{query?}', 'FrontendController@getjoblocation1')->name('getjoblocation1');   
    Route::post('contact/send', 'ContactController@send')->name('contact.send');
    Route::get('fav_job/{query?}', 'FrontendController@fav_job')->name('fav_job');
    Route::get('fav_resume/{query?}', 'FrontendController@fav_resume')->name('fav_resume');
    Route::get('dashboard', 'FrontendController@getDashboard')->name('dashboard');
    Route::get('guided_analysis', 'FrontendController@guided_analysis')->name('guided_analysis');
    Route::get('salary_watch', 'FrontendController@salary_watch')->name('salary_watch');
    Route::get('trends/{caturl}/{catname}', 'FrontendController@trends_datails')->name('trends_datails');
    Route::get('dashboard/{caturl}/{catname}', 'FrontendController@dashboard_datails')->name('dashboard_datails');

    #t Route Start
    Route::get('analysis_guid', 'FrontendController@analysis_guided')->name('analysis_guided');
    Route::get('myjobs/{query?}', 'FrontendController@getjob')->name('myjobs');
    Route::get('getjobmyjobscount/{url}', 'FrontendController@getjobmyjobscount')->name('getjobmyjobscount');
    #t Route End


    Route::group(['namespace' => 'User', 'as' => 'user.'], function () {

        /*
         * User Dashboard Specific
         */
        

        /*
         * User Account Specific
         */
        Route::get('account', 'AccountController@index')->name('account');

        /*
         * User Profile Specific
         */
        Route::patch('profile/update', 'ProfileController@update')->name('profile.update');
    });
});
