
$(document).ready(function() { 
	// toggle skin select	
	$(".span-pp").hide();
var width = $( window ).width();
	$(".main-header #toggle").click(function() { 	
		if($(this).hasClass('active')) {
			$(this).removeClass('active')	
			$('#skin-select').css({"width":"230px"});
			$('.sidebar-menu li a').css({"padding": "12px 5px 12px 15px"});
			$('.wrap-fluid').css({"width":"auto"});
			if(width <= 767){
				$('.wrap-fluid').css({"width":"auto","margin-left":"0px"});
				//$('.dashboard_block').css({"padding-left":"35px"});
			}else{
				$('.wrap-fluid').css({"width":"auto","margin-left":"230px"});
			}			
			$('#skin-select li').css({"text-align":"left"});
			$('#skin-select li span, .devider-title h3, ul.topnav h4, .side-dash, .noft-blue, .noft-purple-number, .noft-blue-number').css({"display":"inline-block", "float":"none"});
			$('.ul.topnav h4, .hide-min-toggle').css({"display":"none"});
			$('.tooltip-tip2').addClass('tooltipster-disable');
			$('.tooltip-tip').addClass('tooltipster-disable');
			$('.datepicker-wrap').css({"position":"absolute", "right":"230px"});
			$('.skin-part').css({"visibility":"visible"});
			$('#menu-showhide').css({"height":"auto", "margin":"-10px 0 0"});
			$('#skin-select li i').css({"top":"8px 0"});
			$('ul.topnav ul').css({"border-radius":"0", "padding":"15px 25px"});
			$('#menuwrapper').removeAttr('id').addClass();
			$(".accordion").addClass('fa-chevron-down');
			$('.mav-head').css({"display":"inline-block"});	
			$('.mav-head1').css({"margin-top":"0px", "margin-bottom":"0px"});
			$('.mav-head1').css({"padding":"9px 15px"});
			$('.skin-part .tooltipster-disable').css({"padding":"0px 0px 0px 10px"});
			$('.support img').css({"width":"40px"});
			
		} else {
			$(this).addClass('active')	
			$('#skin-select').css({"width":"40px"});
			
			$(".accordion").removeClass('fa-chevron-down');
			$(".accordion").removeClass('fa-chevron-up');
			if(width <= 767){
				$('.wrap-fluid').css({"width":"auto","margin-left":"0px"});
				$('.wrap-fluid').css({"padding-left":"45px", "padding-right":"10px"});

			}else{
				$('.wrap-fluid').css({"width":"auto","margin-left":"40px"});
			}
			$('#skin-select li').css({"text-align":"center"});
			$('#skin-select li span, .devider-title h3, ul.topnav h4, .side-dash, .noft-blue, .noft-purple-number, .noft-blue-number').css({"display":"none"});
			//$('.tooltip-tip2').removeClass('tooltipster-disable');
			//$('.tooltip-tip').removeClass('tooltipster-disable');
			$('.datepicker-wrap').css({"position":"absolute", "right":"84px"});	
			$('.skin-part').css({"visibility":"visible"});
			$('.hide-min-toggle').css({"display":"block"});
			$('#menu-showhide').css({"height":"100vh", "margin":"40px 0 0"});
			$('#skin-select li i').css({"top":"0", "left":"-11px"});
			$('ul.topnav ul').css({"border-radius":"0", "padding":"5px 10px"});
			$('ul.topnav ul').removeAttr('style');
			$('#menuwrapper ul li ul').css({"display":"inline-grid!important"});
			$('.accordion-nav').removeAttr('class').addClass('topnav');
			$(".side-bar").attr("id","menuwrapper");
			$('.mav-head').css({"display":"none"});
			$('.mav-head1').css({"margin-top":"60px"});
			$('.mav-head1').css({"margin-bottom":"25px", "height":"58px", "border-bottom":"1px solid #333333", "width": "100%"});
			$('.mav-head1').css({"padding":"9px 6px"});
			$('.skin-part .tooltipster-disable').css({"padding":"0px 0px 0px 5px"});
			$('.support img').css({"width":"30px"});
			$('.sidebar-menu li a').css({"padding":"12px 5px 12px 0px"});
		}
		return false;
	});
	// show skin select for a second
	if(width <= 767){
		setTimeout(function(){ $(".main-header #toggle").removeClass('active').trigger('click'); },10);
	}else{
		setTimeout(function(){ $(".main-header #toggle").addClass('active').trigger('click'); },10);
	}	
}); // end doc.ready
$(".tooltip-tip").click(function(){
	var width = $( window ).width();
	if(width <= 767){
		setTimeout(function(){ $(".main-header #toggle").removeClass('active').trigger('click'); },10);
	}
});

var acc = document.getElementsByClassName("accordion");
var i;
for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function(){
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
	        
        } else {
            panel.style.display = "block";
            $(this).addClass('fa-chevron-down');
            setTimeout(function(){ panel.style.display = "none";   	
			}, 3000);
        }
    }
}

